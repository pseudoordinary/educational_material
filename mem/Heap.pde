class Heap {
  int start_address;
  int size;

  public Heap(String starting_address , int heap_size) {
    //mem_address = pieces[0];
    start_address = convert_from_hex_to_int(starting_address);
    size = heap_size;
  }
  
  int get_start_address()
  {
    return start_address;
  }
  
  int get_size()
  {
    return size;
  }
  
  void set_mem_address(int some_address)
  {
    start_address = some_address;
  }
  
  void set_free_block_size(int some_size)
  {
    size = some_size;
  }
  
  //convert the hex to a integer for starting address
  int convert_from_hex_to_int(String hex)
  {
    return Integer.decode(hex);
  }
  
  //convert from a hex to an integer
  String convert_from_int_to_hex(int integer_to_convert)
  {
   return Integer.toHexString(integer_to_convert); 
  }
  
}