class Record {
  String mem_address;
  int free_block_size;
  int lines_in_the_block;
  int mem_address_in_decimal;
  int free_is_zero_not_is_one;

 public Record(int mem_row_size) {
    //init everything to zero
    
    mem_address = "";
    free_block_size = 0;
    lines_in_the_block = free_block_size/mem_row_size;
    if(free_block_size % mem_row_size != 0)
    {
      lines_in_the_block++;
    }
    mem_address_in_decimal = convert_from_hex_to_int(mem_address);
    free_is_zero_not_is_one = 0;
  }
  
  //overwritten
  public Record(String[] pieces, int mem_row_size) {
    mem_address = pieces[0];
    free_block_size = Integer.parseInt(pieces[1]);
    lines_in_the_block = free_block_size/mem_row_size;
    if(free_block_size % mem_row_size != 0)
    {
      lines_in_the_block++;
    }
    mem_address_in_decimal = convert_from_hex_to_int(mem_address);
    free_is_zero_not_is_one = 0;
  }
  
   public Record(String address, int blocks, int mem_row_size) {
    //init everything to zero
    
    mem_address = address;
    free_block_size = blocks;    
    lines_in_the_block = free_block_size/mem_row_size;
    if(free_block_size % mem_row_size != 0)
    {
      lines_in_the_block++;
    }
    mem_address_in_decimal = convert_from_hex_to_int(mem_address);
    free_is_zero_not_is_one = 0;
  }
  
  String get_mem_address()
  {
    return mem_address;
  }
  
  int get_free_block_size()
  {
    return free_block_size;
  }
  
  int get_lines_in_the_block()
  {
    return lines_in_the_block;
  }
  
  int get_mem_address_in_decimal()
  {
    return mem_address_in_decimal;
  }
  
  int get_free_is_zero_not_is_one()
  {
    return free_is_zero_not_is_one;
  }
  
  void set_mem_address(String some_address)
  {
    mem_address = some_address;
  }
  
  void set_free_block_size(int some_size)
  {
    free_block_size = some_size;
  }
  
  void set_lines_in_the_block(int lines)
  {
    lines_in_the_block = lines;
  }
  
  void set_mem_address_in_decimal(int address)
  {
    mem_address_in_decimal = address;
  }
 
  void set_free_is_zero_not_is_one(int free)
  {
    free_is_zero_not_is_one = free;
  }
 
  //convert the hex to a integer for starting address
  int convert_from_hex_to_int(String hex)
  {
    if(hex != null && !hex.isEmpty() )
    {
      //you want to get rid of the first two letters 0x
      String hex_get_rid_beginning = hex.substring(2, hex.length());
      int decimal = Integer.parseInt(hex_get_rid_beginning,16);
    return decimal;
    }
    return -1;
  }
}