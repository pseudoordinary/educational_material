Record[] records; // heap  //<>//
Record[] records_copy_for_links; // copy of the array for linking the ones which are free
Record[] ordered_records_with_filled_in_values_for_occupied_spaces = new Record[500]; // ordered records from the file input
String[] lines; //file input
int recordCount; //amount of blocks
PFont body;
int num = 1; // Display this many entries on each screen.
int startingEntry = 0;  // Display from this entry number
String free_list_type; //Type of free list
int mem_row_size; // Size of each memory row
int total_heap_size; // Size of total heap
String time_and_date; // Time and date the snapshot was taken
int put_into_counter = 0; //elements in the ordered array
int free_counter = 1;//start off at one

void setup() {
  size(600, 600);
  fill(255);
  noLoop();

  //load in file
  lines = loadStrings("mem.txt");
  records = new Record[lines.length]; // subtract three, one for first line, space, and time/date
  records_copy_for_links = new Record[lines.length];

  //first line is "Explicit MemRowSize TotalHeapSize"
  String[] top_line = split(lines[0], ' ');
  free_list_type = top_line[0];
  free_list_type = top_line[0];
  mem_row_size = Integer.parseInt(top_line[1]);
  total_heap_size = Integer.parseInt(top_line[2]);

  //second line is space - do nothing

  //third line is # 8/23/12 - 12:40AM
  time_and_date = lines[2]; //contains the hash tag

  for (int i = 3; i < lines.length; i++) {
    String[] pieces = split(lines[i], ' '); // Load data into array
    if (pieces.length == 2) {
      records[recordCount] = new Record(pieces, mem_row_size);
      records_copy_for_links[recordCount] = new Record(pieces, mem_row_size);
      recordCount++;
    }
  }

  //after data is loaded in, we want it to be ordered
  order_mem_by_decimal_values(records);

  //put in allocated blocks
  set_allocated_into_array(records, ordered_records_with_filled_in_values_for_occupied_spaces);
}

void draw() {

  background(0);
  textSize(15);
  fill(255, 255, 255);
  text( "Memory Visualizer Tool - Blocks are contigious, click for the next block", 20, 20);
  text("Snap shot was taken: "+time_and_date + "      Red is Free, Blue is not Free\nIf block is too large, footer will be cut off by screen size!", 30, 570);
  String s = new String();
  String temp = new String();
  int counter_per_block_to_display = 0;
  for (int i = 0; i < num; i++) { // each block
    int thisEntry = startingEntry + i;
    println("thisEntry: "+thisEntry);
    println("put_into_counter: "+ put_into_counter);
    println("free_counter: "+ free_counter);
    
    println("length of the record copy array: "+records_copy_for_links.length);
        println("length: "+records_copy_for_links[0]);
            println("length: "+records_copy_for_links[1]);
                println("length: "+records_copy_for_links[2].get_mem_address());

    
    //once you reach the last entry, so the free_counter is equal to the length of the copy of the array-4, then you want to reset the counter so that it works for the next time the loops occurs
    if(free_counter == (records_copy_for_links.length-2))
    {
     free_counter = 2;
    }
    else
    {
     //do nothing
    }

    //    if(thisEntry == (put_into_counter-1))
    //    {
    //     println("how often here?");
    //     //reset free_counter;
    //     free_counter = 1;
    //    }

    if (thisEntry<put_into_counter) 
    { // if this block is less than the total number of blocks in the heap
      temp = "\n________Start Address of Block in Hex: "  + ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].mem_address + "___________________" + "\n";

      if (counter_per_block_to_display == 0)
      {
        //this is the first line for the block, so we want it to be the requested size, 
        temp = temp + "|_______Requested Size (Bytes)________|_____Block Size: "+ ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_block_size() +"____|_00a_| \n";
        counter_per_block_to_display++;
      } else
      {
      }


      //this is the payload + padding so get the number newlines asciirecords_copy_for_link.length - 3
      for (int j = 1; j < ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].lines_in_the_block -1; j++ ) {
        if (j == 1)
        {
          temp = temp + "| Payload + Padding ="+ (ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_block_size() - (j*mem_row_size)) +"bytes  ";
          if (ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_is_zero_not_is_one() == 0 && (free_counter < (records_copy_for_links.length - 3)))
          {
            //if the block is free, append the link
            
           // temp = temp + "| Link to next free block: " + records_copy_for_links[free_counter].get_mem_address() + " |\n";
            free_counter = free_counter + 1;            
            
          } else
          {
            //the last free block, mention it!
            if (ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_is_zero_not_is_one() == 0 && (free_counter == (records_copy_for_links.length - 3)))
            {
              //this means that the end is here for the frees
              temp = temp + "  | Last free block, so no more links! |\n";
            } else
            {
              //if the block is not free, dont append the link
              temp = temp + "                                                   |\n";
            }
          }
        } else
        {
          temp = temp + "|                                                                                                         |\n";
        }
      }


      //this is the footer
      temp = temp + "|_____End of Block Unused Bytes______|_____Block Size: "+ ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_block_size() +"____|_00a_| \n";

      temp = temp+ "|If only header & footer appears, then space too small for payload.|";

      //if the block is free, then you want to check if the block has space less than or equal to header+ footer size, this means that there is external fragmentation for free space ASSUMPTION THAT THE HEADER AND FOOTER IS ONE ROW SIZE
      if ( ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_is_zero_not_is_one() == 0 && ( ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_block_size() <= (mem_row_size*2) ))
      {
        temp = temp + "\n\n| External fragmentation of this block in the free list is: "+ ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_block_size() +" bytes  |\n" + 
        "\n External Frag, means no space, thus link is not shown in the picture";

        //TODO Next free block for extern frag items 
        //TODO RESET THE FREE_COUNTER AFTER THE ENTIRE THing

        free_counter = free_counter + 1;
      } else
      {
      }

      counter_per_block_to_display++;

      s = s + temp;


      //mention if it was free memory
      if (ordered_records_with_filled_in_values_for_occupied_spaces[thisEntry].get_free_is_zero_not_is_one() == 0)
      {
        //memory is free
        fill(253, 0, 0);
      } else {
        //memory is not free
        fill(0, 0, 253);
      }

      //fill(0, 0, 0);
      text(temp, 30, 30+ (30*i));
    }
  }

  text(s, 20, 50, 575, 575);
}

void mousePressed() {
  startingEntry += num; 
  if (startingEntry > records.length-2) {
    startingEntry = 0;  // go back to the beginning
  } 
  redraw();
}


void keyPressed()
{
  if (keyCode == UP) {
    // Do up up stuff here
  }
}


//return -1 if bad inputs
int error_check()
{
  int bytes_total = 0;
  //loop through the array and check if the number of bytes match up to the heap size
  for (int i = 0; i < records.length; i++)
  {
    bytes_total = bytes_total + records[i].free_block_size;
  }

  //remember to remove the less than later when finished! this is just for making testing easier
  if (bytes_total <= total_heap_size)
  {
    //this is good 
    return 0;
  } else
  {
    return -1;
  }
}

//orders the  array with insertion sort, do not care about efficiency
void order_mem_by_decimal_values(Record[] record_array_to_order)
{ 
  Record temp; 
  int lowest_address_value = 0;
  int index_of_lowest_value = 0;
  for (int i=0; i<record_array_to_order.length-3; i++) 
  {
    lowest_address_value = record_array_to_order[i].mem_address_in_decimal;
    index_of_lowest_value = i;

    for (int j = i; j < record_array_to_order.length-3; j++)
    {
      if (record_array_to_order[j].mem_address_in_decimal < lowest_address_value)
      {
        lowest_address_value = record_array_to_order[j].get_mem_address_in_decimal();
        index_of_lowest_value = j;
      }
    }

    //swap i and index
    temp = new Record(mem_row_size);
    temp.set_mem_address(record_array_to_order[i].get_mem_address());
    temp.set_free_block_size(record_array_to_order[i].get_free_block_size());
    temp.set_lines_in_the_block(record_array_to_order[i].get_lines_in_the_block());
    temp.set_mem_address_in_decimal(record_array_to_order[i].get_mem_address_in_decimal());
    temp.set_free_is_zero_not_is_one(record_array_to_order[i].get_free_is_zero_not_is_one());

    record_array_to_order[i].set_mem_address(record_array_to_order[index_of_lowest_value].get_mem_address());     //i=lowest;
    record_array_to_order[i].set_free_block_size(record_array_to_order[index_of_lowest_value].get_free_block_size());
    record_array_to_order[i].set_lines_in_the_block(record_array_to_order[index_of_lowest_value].get_lines_in_the_block());
    record_array_to_order[i].set_mem_address_in_decimal(record_array_to_order[index_of_lowest_value].get_mem_address_in_decimal());
    record_array_to_order[i].set_free_is_zero_not_is_one(record_array_to_order[index_of_lowest_value].get_free_is_zero_not_is_one());

    record_array_to_order[index_of_lowest_value].set_mem_address(temp.get_mem_address());
    record_array_to_order[index_of_lowest_value].set_free_block_size(temp.get_free_block_size());
    record_array_to_order[index_of_lowest_value].set_lines_in_the_block(temp.get_lines_in_the_block());
    record_array_to_order[index_of_lowest_value].set_mem_address_in_decimal(temp.get_mem_address_in_decimal());
    record_array_to_order[index_of_lowest_value].set_free_is_zero_not_is_one(temp.get_free_is_zero_not_is_one());
  }
}

//have all the free blocks order, gaps in the array are the allocated blocks, make the array have the allocated blocks too
void set_allocated_into_array(Record[] record_to_add, Record[] record_to_put_into)
{
  //int put_into_counter = 0;
  //loop through the array that you want to add to
  for (int i = 0; i < record_to_add.length - 3; i++)
  {

    //last element, just throw it in
    if (i+1 >= record_to_add.length-3)
    {
      //just put the last dude in
      //only put in this dude
      record_to_put_into[put_into_counter] = record_to_add[i];
      record_to_put_into[put_into_counter].set_free_is_zero_not_is_one(0);
      put_into_counter++;

      //afterwards, you need to check if there is an allocated space there too.

      //loop through all the stuff, add the block sizes, if less than the total heap size, make a final block that is allocated
      int counter_total_block_sizes = 0;
      for (int g = 0; (g < put_into_counter); g++)
      {

        counter_total_block_sizes = counter_total_block_sizes + record_to_put_into[g].get_free_block_size();

        if (g == put_into_counter-1)
        {

          //check the size of the total blocksizes

          if (counter_total_block_sizes < total_heap_size)
          {
            //less than the total heap size, there is an allocated block at the end
            //get the hex address = (hex)(prev_address + prev_block_size)
            int prev_plus_block_size = record_to_add[i].get_mem_address_in_decimal() + record_to_add[i].get_free_block_size();
            String hex_string = convert_from_int_to_hex(prev_plus_block_size);
            //get the block size
            //get the next block address and subtract it from this address
            int block_size_of_allocated = total_heap_size - counter_total_block_sizes; //records[i+1].get_mem_address_in_decimal() - prev_plus_block_size;
            Record temp = new Record(hex_string, block_size_of_allocated, mem_row_size);
            record_to_put_into[put_into_counter] = temp;
            record_to_put_into[put_into_counter].set_free_is_zero_not_is_one(1);
            put_into_counter++;
          } else
          {
          }
        }
      }
    } else {
      //if the address + bytes is ssmaller than the next address
      if (record_to_add[i].get_mem_address_in_decimal() + record_to_add[i].get_free_block_size() < record_to_add[i+1].get_mem_address_in_decimal())
      {
        //put in the first guy
        record_to_put_into[put_into_counter] = records[i];
        record_to_put_into[put_into_counter].set_free_is_zero_not_is_one(0);
        put_into_counter++;
        //put in the allocated block guy
        //get the hex address = (hex)(prev_address + prev_block_size)
        int prev_plus_block_size = record_to_add[i].get_mem_address_in_decimal() + record_to_add[i].get_free_block_size();
        String hex_string = convert_from_int_to_hex(prev_plus_block_size);
        //get the block size
        //get the next block address and subtract it from this address
        int block_size_of_allocated = records[i+1].get_mem_address_in_decimal() - prev_plus_block_size;
        Record temp = new Record(hex_string, block_size_of_allocated, mem_row_size);
        record_to_put_into[put_into_counter] = temp;
        record_to_put_into[put_into_counter].set_free_is_zero_not_is_one(1);
        put_into_counter++;
      } else
      {
        if (record_to_add[i].get_mem_address_in_decimal() + record_to_add[i].get_free_block_size() == record_to_add[i+1].get_mem_address_in_decimal())
        {
          //only put in this dude
          record_to_put_into[put_into_counter] = record_to_add[i];
          put_into_counter++;
        } else
        {
          // > so there is an error in the data! TODO figure out what to do later
        }
      }
    }
  }
}

//convert from a hex to an integer
String convert_from_int_to_hex(int integer_to_convert)
{
  return Integer.toHexString(integer_to_convert);
}