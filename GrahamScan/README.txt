READ ME file for GrahamScan Program

In order to run the program:
1. Open up netbeans or eclipse.
2. Run the program
3. When the program runs, a popup window will prompt for a file to be run.
4. Place a file with input into the SAME file as the entire project
5. Type in the file name. Make sure that the .txt is inputted.
6. Click Okay or press enter.
7. Another popup will ask for a desired file name for the output file.
8. Type in a desired file name. Remember to put in the .txt!
9. The file is in the project file. The same place as the input files

The program worked for both the sample inputs that were provided.
It should work for the degenerate case where there are colinear points and
if there are points that are repeat values.

The output values will be the bottom right point going counter clockwise.