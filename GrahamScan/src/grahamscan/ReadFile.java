package grahamscan;

import java.io.File;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Miuki
 */
public class ReadFile {

    private Scanner x;
    private PointArray array;

    public void openFile(String file) {
        try {
            x = new Scanner(new File(file));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "You have an incorrect file name! Did you remember to put in the .txt extension?");

        }
    }

    public void readFile() {
        int counterPoints = 0;
        while (x.hasNextLine()) {

            //while the file has a next line...do the following
            String a = x.nextLine();

            if (counterPoints == 0) {
                //the first line of the text document should be the number of points
                //that will be in the array
                array = new PointArray(Integer.parseInt(a));

                counterPoints++;
            } else {
                //all the lines afterwards will be the lines that contain the points. 
                //break up the points and hten set the current iteration (counter) of it in the
                //array wit hthe corrct x and y value

                int commaNum = a.indexOf(",");

                String xValue = a.substring(1, commaNum);
                String yValue = a.substring(commaNum + 1, a.length() - 1);

                int xInt = Integer.parseInt(xValue);
                int yInt = Integer.parseInt(yValue);

                //insert data into the points on the arraylist
                array.setPoint(counterPoints - 1, xInt, yInt);

                counterPoints++;
            }
        }
    }

    public void closeFile() {
        x.close();
    }

    public PointArray getArray() {
        return array;
    }

}
