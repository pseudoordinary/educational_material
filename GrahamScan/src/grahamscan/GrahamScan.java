package grahamscan;

import java.io.File;
import java.util.Scanner;
import java.util.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Miuki
 */
public class GrahamScan {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        ReadFile r = new ReadFile();

        String fileName = JOptionPane.showInputDialog("Input the name of your file! "
                + "\nPlease have the file within the GrahamScan folder. "
                + "\nDon't move it into an of the files within the GrahamScan folder. "
                + "\nRemember to input the file extension too! (.txt) ");

        r.openFile(fileName);
        r.readFile();
        r.closeFile();

        r.getArray().sortArrayListBasedOnY();
        r.getArray().getBottomRightPointAsIndexZero();

        r.getArray().createAngleArray();

        r.getArray().sortArrayListBasedOnAngles();

        r.getArray().createHullPoints();

        //write files
        WriteFile w = new WriteFile();

        String writeFileName = JOptionPane.showInputDialog("Input the desired name of your output file! "
                + "\nPlease check for the file within the GrahamScan folder. "
                + "\nDon't move it into an of the files within the GrahamScan folder. "
                + "\nRemember to input the file extension too! (.txt) ");

        w.openFile(writeFileName);

        //print the size of the hull
        w.getX().format("%d", r.getArray().getHullPoints().size());

        //loop through the hull list and print
        int counter = 0;
        while (counter < r.getArray().getHullPoints().size()) {

            w.getX().format("%n%s%d%s%d%s", "[", r.getArray().getHullPoints().get(counter).getX(), ",", r.getArray().getHullPoints().get(counter).getY(), "]");
            counter++;
        }

        w.closeFile();

    }

}
