package grahamscan;

import java.io.*;
import java.lang.*;
import java.util.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Miuki
 */
public class WriteFile {

    private Formatter x;

    public Formatter getX() {
        return x;
    }

    public void openFile(String fileName) {
        try {
            x = new Formatter(fileName);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "You have an incorrect file name! Did you remember to put in the .txt extension?");

        }

    }

    public void closeFile() {
        x.close();
    }

}
