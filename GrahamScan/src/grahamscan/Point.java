package grahamscan;

/**
 *
 * @author Miuki
 */
public class Point {

    private int x;
    private int y;
    private double angleInRelationToOriginal;

    public Point() {
    }

    public Point(int xValue, int yValue) {
        x = xValue;
        y = yValue;

    }

    public Point(int xValue, int yValue, double angle) {
        x = xValue;
        y = yValue;
        angleInRelationToOriginal = angle;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getAngleInRelationToOriginal() {
        return angleInRelationToOriginal;
    }

    public void setAngleInRelationToOriginal(double angleInRelationToOriginal) {
        this.angleInRelationToOriginal = angleInRelationToOriginal;
    }

}
