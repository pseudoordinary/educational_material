package grahamscan;

import java.util.ArrayList;

/**
 *
 * @author Miuki
 */
public class PointArray {

    private ArrayList<Point> arrayList;
    private double[] angles;
    private ArrayList<Point> hullPoints;

    public double[] getAngles() {
        return angles;
    }

    public void setArrayList(ArrayList<Point> arrayList) {
        this.arrayList = arrayList;
    }

    public void setAngles(double[] angles) {
        this.angles = angles;
    }

    public void setHullPoints(ArrayList<Point> hullPoints) {
        this.hullPoints = hullPoints;
    }

    public ArrayList<Point> getHullPoints() {
        return hullPoints;
    }

    public PointArray() {
    }

    public PointArray(int numOfSpaces) {
        arrayList = new ArrayList<Point>(numOfSpaces);
    }

    public ArrayList<Point> getArrayList() {
        return arrayList;
    }

    public Point getPoint(int space) {
        return arrayList.get(space);
    }

    public int getLength() {
        return arrayList.size();
    }

    public void setPoint(int cellNum, int xValue, int yValue) {
        Point pointToAddToArrayList = new Point(xValue, yValue);
        arrayList.add(cellNum, pointToAddToArrayList);

    }

    public void swap(int swap1, int swap2) {

        //store the value from 1 into a dummy variable
        Point hold1 = new Point(arrayList.get(swap1).getX(), arrayList.get(swap1).getY());

        //set the first value to what the second value is
        arrayList.get(swap1).setX(swap2);
        arrayList.get(swap1).setY(swap2);

        //set the second value to what the dummy variablw was
        arrayList.get(swap2).setX(hold1.getX());
        arrayList.get(swap2).setY(hold1.getY());

    }

    public void sortArrayListBasedOnY() {

        //keep track of lowest values
        int lowestYValue = 0;
        int indexOfLowestY = 0;

        //pick a point
        for (int i = 0; i < arrayList.size(); i++) {

            lowestYValue = arrayList.get(i).getY();
            indexOfLowestY = i;

            //      System.out.println(arrayList.size());
            for (int j = i; j < arrayList.size(); j++) {
                if (arrayList.get(j).getY() < lowestYValue) {
                    lowestYValue = arrayList.get(j).getY();
                    indexOfLowestY = j;
                }
            }

            //swap i and index
            Point temp = new Point();
            temp.setX(arrayList.get(i).getX());       //temp = i;
            temp.setY(arrayList.get(i).getY());

//          temp = arrayList.get(i);
            arrayList.get(i).setX(arrayList.get(indexOfLowestY).getX());      //i=lowest;
            arrayList.get(i).setY(arrayList.get(indexOfLowestY).getY());

            arrayList.get(indexOfLowestY).setX(temp.getX());
            arrayList.get(indexOfLowestY).setY(temp.getY());

        }

    }

//make sure that the array is already sorted
    public void getBottomRightPointAsIndexZero() {
        int yToLookFor = arrayList.get(0).getY();

        int counter = 1;

        Point temp = new Point();
        temp = arrayList.get(0);
        int tempIndex = 0;

        //compare the initial y value in the zero index to the ones after if it has the same value
        while (arrayList.get(counter).getY() == yToLookFor) {
            //compare the x values to get the largest x
            if (arrayList.get(counter).getX() > temp.getX()) {
                // if the x value is larger, then change the temp variable
                temp = arrayList.get(counter);
                tempIndex = counter;
            }
            counter++;
        }

        //once all the y value variables with the largest x has been found, swap the two values
        //make the temp value the first value
        Point forNow = new Point();
        forNow.setX(arrayList.get(tempIndex).getX());
        forNow.setY(arrayList.get(tempIndex).getY());

        arrayList.get(tempIndex).setX(arrayList.get(0).getX());
        arrayList.get(tempIndex).setY(arrayList.get(0).getY());

        //make the zero index, the temp value
        arrayList.get(0).setX(forNow.getX());
        arrayList.get(0).setY(forNow.getY());

    }

    public void createAngleArray() {
        angles = new double[arrayList.size() + 1];

        double dividedNum;
        double tanInverse;
        double numerator;
        double denominator;

        //creating the array
        angles[0] = 99999999.99;
        for (int i = 1; i < arrayList.size(); i++) {
            if ((arrayList.get(i).getX() - arrayList.get(0).getX()) == 0) {
                angles[i] = 1.57079633;
            } else {
                numerator = arrayList.get(i).getY() - arrayList.get(0).getY();
                denominator = arrayList.get(i).getX() - arrayList.get(0).getX();

                dividedNum = numerator / denominator;

                tanInverse = Math.atan(dividedNum);

                if (tanInverse < 0) {
                    tanInverse = tanInverse + 3.14159265;
                }

                angles[i] = tanInverse;

                if (numerator == 0) {
                    angles[i] = 3.14159265;
                }
            }
        }

        for (int g = 0; g < angles.length - 1; g++) {
            arrayList.get(g).setAngleInRelationToOriginal(angles[g]);
        }

    }

    public void sortPointsAccordingToAngle() {

        //keep track of lowest values
        double lowestAngle = 0;
        int indexOfLowestAngle = 0;

        //pick a point
        for (int i = 1; i < arrayList.size(); i++) {

            lowestAngle = arrayList.get(i).getAngleInRelationToOriginal();
            indexOfLowestAngle = i;

            for (int j = i; j < arrayList.size(); j++) {
                if (arrayList.get(j).getAngleInRelationToOriginal() < lowestAngle) {
                    lowestAngle = arrayList.get(j).getAngleInRelationToOriginal();
                    indexOfLowestAngle = j;
                }
            }

            //swap i and index
            Point temp = new Point();
            temp.setX(arrayList.get(i).getX());       //temp = i;
            temp.setY(arrayList.get(i).getY());

//          temp = arrayList.get(i);
            arrayList.get(i).setX(arrayList.get(indexOfLowestAngle).getX());      //i=lowest;
            arrayList.get(i).setY(arrayList.get(indexOfLowestAngle).getY());

            arrayList.get(indexOfLowestAngle).setX(temp.getX());
            arrayList.get(indexOfLowestAngle).setY(temp.getY());

        }

    }

    public void sortArrayListBasedOnAngles() {

        //keep track of lowest values
        double lowestAngle = 0.0;
        int indexOfLowestAngle = 0;

        //pick a point
        for (int i = 1; i < arrayList.size(); i++) {

            lowestAngle = arrayList.get(i).getAngleInRelationToOriginal();
            indexOfLowestAngle = i;

            //      System.out.println(arrayList.size());
            for (int j = i; j < arrayList.size(); j++) {
                if (arrayList.get(j).getAngleInRelationToOriginal() < lowestAngle) {
                    lowestAngle = arrayList.get(j).getAngleInRelationToOriginal();
                    indexOfLowestAngle = j;
                }
            }

            //swap i and index
            Point temp = new Point();
            temp.setAngleInRelationToOriginal(arrayList.get(i).getAngleInRelationToOriginal());
            temp.setX(arrayList.get(i).getX());       //temp = i;
            temp.setY(arrayList.get(i).getY());

//          temp = arrayList.get(i);
            arrayList.get(i).setAngleInRelationToOriginal(arrayList.get(indexOfLowestAngle).getAngleInRelationToOriginal());
            arrayList.get(i).setX(arrayList.get(indexOfLowestAngle).getX());      //i=lowest;
            arrayList.get(i).setY(arrayList.get(indexOfLowestAngle).getY());

            arrayList.get(indexOfLowestAngle).setAngleInRelationToOriginal(temp.getAngleInRelationToOriginal());
            arrayList.get(indexOfLowestAngle).setX(temp.getX());
            arrayList.get(indexOfLowestAngle).setY(temp.getY());

        }

    }

//if result is positive, it is turning left (what we want)
//if the result is negative, it is turning right, what we dont wnat
//if the result is zero, or if two points are referencing the same point, then the result is colinear
    public double orientationTest(Point p, Point q, Point r) {
        double resultOfTest = 0.0;

        double firstValue = (q.getX() * r.getY()) - (q.getY() * r.getX());
        double secondValue = p.getX() * (r.getY() - q.getY());
        double thirdValue = p.getY() * (r.getX() - q.getX());

        resultOfTest = firstValue - secondValue + thirdValue;

        return resultOfTest;
    }

    public void createHullPoints() {

        hullPoints = new ArrayList<Point>();

        //add the first two points
        hullPoints.add(arrayList.get(0));
        hullPoints.add(arrayList.get(1));

        //keep adding points to the list, until you hit a orientation test that goes to the right
        //when the point turns right, remove the previous point. then check the past points with this one.
        //try a while loop
        int counter = 2;
        while (counter < arrayList.size()) {
            Point hullPointTemp1 = new Point();
            hullPointTemp1 = hullPoints.get(hullPoints.size() - 2);
            Point hullPointTemp2 = new Point();
            hullPointTemp2 = hullPoints.get(hullPoints.size() - 1);

            if (orientationTest(hullPointTemp1, hullPointTemp2, arrayList.get(counter)) <= 0) {
                hullPoints.remove(hullPoints.size() - 1);
                hullPoints.trimToSize();

                counter--;

            } else {
                hullPoints.add(arrayList.get(counter));

            }

            counter++;

        }

    }

}
