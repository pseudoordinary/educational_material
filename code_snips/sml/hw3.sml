(*Variables for testing*)
val coin_cap = 5;
val refill_amt = 2;
val coins_present = 5;
val some_list = [(1,1),(2,2),(3,4),(4,3)];
val booth_number = 1;

(* Comment: Helper methods *)
fun multiply(num1, num2):int= 
	num1 * num2;

fun add(num1, num2):int= 
	num1 + num2;

fun subtract(num1, num2):int= 
	num1 - num2;

fun add_refill_to_coins_now(cap, refill, coins_now):int=
	if(add(refill,coins_now) > cap) then cap
	else add(refill,coins_now);

fun getLength(list):int=
	if(list = []) then 0
	else 1 + getLength(tl(list));

fun getFun(booth_list: (int*int) list,booth_num):int=
	if(getLength(booth_list) = 0) then 0
	else if((#1(hd(booth_list)) = (booth_num))) then #2(hd(booth_list))
		else getFun(tl(booth_list),booth_num);
	

(* Find the max fun *)
fun getMaxFun(booth_list, cap, refill, now_tokens, booth_num, has_play):int=

(*BASE CASES*)

	if((now_tokens = 0) andalso (booth_num = getLength(booth_list))) then 0
		else if((getLength(booth_list) = 0)) then 0
			else if(getLength(booth_list) = 1) 
				then multiply(now_tokens, getFun(booth_list, booth_num))
				else if(now_tokens = 0) then getMaxFun(tl(booth_list), cap, refill, add_refill_to_coins_now(cap, refill, now_tokens), add(booth_num, 1), 0)

(*DONE WITH CHECKING BASE CASES*)
					else if(has_play = 0) then add(getFun(booth_list, booth_num), getMaxFun(booth_list, cap, refill, subtract(now_tokens, 1), booth_num, 1))
						
						else if( add(getFun(booth_list, booth_num), getMaxFun(booth_list, cap, refill, subtract(now_tokens,1), booth_num, 1))
						> (getMaxFun(tl(booth_list), cap, refill, add_refill_to_coins_now(cap, refill, now_tokens), add(booth_num, 1), 0))) 
						then add(getFun(booth_list, booth_num), getMaxFun(booth_list, cap, refill, subtract(now_tokens, 1), booth_num, 1))
							else getMaxFun( tl(booth_list), cap, refill, add_refill_to_coins_now(cap, refill, now_tokens), add(booth_num, 1), 0 );  


(*Wrapper function so that it is the correct inputs*)
fun total_fun(NumberGames, Cap, Refill, ListGameFuns):int= getMaxFun(ListGameFuns, Cap, Refill, Cap, 1, 0);


(*Run Methods*)

(* multiply(num1,num2);
add(num1,num2);
subtract(num1,num2);
add_refill_to_coins_now(coin_cap, refill_amt, coins_present);
getLength(some_list);
getFun(some_list, booth_number)
getMaxFun(some_list, coin_cap, refill_amt, coins_present, booth_number, 0)
total_fun(4, 5, 2, [(1,1),(2,2),(3,4),(4,3)]);
*)
