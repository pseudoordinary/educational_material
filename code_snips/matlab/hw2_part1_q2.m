%Project 2 Part 1 Question 2

%Initializing variables
XcIrna = 1.2; %s^-1
XcIprot = 1.2; %s^-1

Xcrorna = 0.8; %s^-1 
Xcroprot = 0.8; %s^-1

omegacI = 50; %s^-1
mucI = 50; %s^-1
omegacro = 50; %s^-1myi
mucro = 50; %s^-1

kcIHalf = 10; %molecules/cell * s^-1
kcroHalf = 10; %molecules/cell * s^-1

totalTime = 25; %s
delta_t = .01; %s
numsteps = totalTime/delta_t;

%arrays
cIprotein = zeros(numsteps,1);
cIrna = zeros(numsteps,1);
croprotein = zeros(numsteps,1);
crorna = zeros(numsteps,1);
time = zeros(numsteps, 1);

%Initial values
cIprotein(1) = 0; %mM
croprotein(1) = 0;
cIrna(1) = 0;
crorna(1) = 0;

%starting concentrations
totalConcentrations = 20; %s
delta_t_cons = 1; %s
numsteps_cons = totalConcentrations/delta_t_cons;

figure;
hold on;
for r = 0:delta_t_cons:totalConcentrations %crorna values
    for p = 0:delta_t_cons:totalConcentrations %cirna values

        %initial value
        crorna(1) = r; %mM
        cIrna(1) = p;
                
        %start the loop
        for i = 1: numsteps-1
            dcIProtein_dt = ((omegacI * cIrna(i)) - (XcIprot*cIprotein(i)));
            dcIRNA_dt = ((mucI*(1-((croprotein(i)^2)/((kcroHalf^2) + (croprotein(i)^2))))) - (XcIrna*(cIrna(i))));
            dcroProtein_dt = ((omegacro * crorna(i)) - (Xcroprot*croprotein(i)));
            dcroRNA_dt = ((mucro*(1-((cIprotein(i)^2)/((kcIHalf^2) + (cIprotein(i)^2))))) - (Xcrorna*(crorna(i))));

            %increment and save with the d/dt values
            cIprotein(i+1) = cIprotein(i) + (dcIProtein_dt*delta_t);
            cIrna(i+1) = cIrna(i) + (dcIRNA_dt*delta_t);
            croprotein(i+1) = croprotein(i) + (dcroProtein_dt*delta_t);
            crorna(i+1) = crorna(i) + (dcroRNA_dt*delta_t);
        end    
        plot(croprotein,cIprotein);
    end
        %if (r==5)
        %   plot(croprotein,cIprotein);
        %end;
end


title('[croprotein] vs [ciprotein]')
xlabel('croprotein concentration')
ylabel('ci protein concentration (mM)')


%Initializing variables
XcIrna = 1.2; %s^-1
XcIprot = 1.2; %s^-1

Xcrorna = 0.8; %s^-1 
Xcroprot = 0.8; %s^-1

omegacI = 50; %s^-1
mucI = 50; %s^-1
omegacro = 50; %s^-1myi
mucro = 50; %s^-1

kcIHalf = 10; %molecules/cell * s^-1
kcroHalf = 10; %molecules/cell * s^-1

totalTime = 25; %s
delta_t = .01; %s
numsteps = totalTime/delta_t;

%arrays
cIprotein = zeros(numsteps,1);
cIrna = zeros(numsteps,1);
croprotein = zeros(numsteps,1);
crorna = zeros(numsteps,1);
time = zeros(numsteps, 1);

%Initial values
cIprotein(1) = 0; %mM
croprotein(1) = 0;
cIrna(1) = 0;
crorna(1) = 0;

%starting concentrations
totalConcentrations = 2000; %s
delta_t_cons = 500; %s
numsteps_cons = totalConcentrations/delta_t_cons;

figure;
hold on;
for r = 0:delta_t_cons:totalConcentrations %crorna values
    for p = 0:delta_t_cons:totalConcentrations %cirna values

        %initial value
        crorna(1) = r; %mM
        cIrna(1) = p;
                
        %start the loop
        for i = 1: numsteps-1
            dcIProtein_dt = ((omegacI * cIrna(i)) - (XcIprot*cIprotein(i)));
            dcIRNA_dt = ((mucI*(1-((croprotein(i)^2)/((kcroHalf^2) + (croprotein(i)^2))))) - (XcIrna*(cIrna(i))));
            dcroProtein_dt = ((omegacro * crorna(i)) - (Xcroprot*croprotein(i)));
            dcroRNA_dt = ((mucro*(1-((cIprotein(i)^2)/((kcIHalf^2) + (cIprotein(i)^2))))) - (Xcrorna*(crorna(i))));

            %increment and save with the d/dt values
            cIprotein(i+1) = cIprotein(i) + (dcIProtein_dt*delta_t);
            cIrna(i+1) = cIrna(i) + (dcIRNA_dt*delta_t);
            croprotein(i+1) = croprotein(i) + (dcroProtein_dt*delta_t);
            crorna(i+1) = crorna(i) + (dcroRNA_dt*delta_t);
        end    
        plot(croprotein,cIprotein);
    end
        
end


title('[croprotein] vs [ciprotein]')
xlabel('croprotein concentration')
ylabel('ci protein concentration (mM)')
