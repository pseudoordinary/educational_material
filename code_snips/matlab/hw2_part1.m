%Project 2 Part 1

%Initializing variables
XcIrna = 1.2; %s^-1
XcIprot = 1.2; %s^-1

Xcrorna = 0.8; %s^-1 
Xcroprot = 0.8; %s^-1

omegacI = 50; %s^-1
mucI = 50; %s^-1
omegacro = 50; %s^-1
mucro = 50; %s^-1

kcIHalf = 10; %molecules/cell * s^-1
kcroHalf = 10; %molecules/cell * s^-1

totalTime = 20; %s
%delta_t = 1;
delta_t = 0.01; %s
numsteps = totalTime/delta_t;

%arrays
cIprotein = zeros(numsteps,1);
cIrna = zeros(numsteps,1);
croprotein = zeros(numsteps,1);
crorna = zeros(numsteps,1);
time = zeros(numsteps, 1);


%Run three simulations, 
%one with all concentrations equal to 0, 
%one with 20 molecules of cro RNA present, 
%and one with 50 molecules of cI present.  
%Present your results as concentrations
%of all components versus time.

%Initial values
cIprotein(1) = 0; %mM
cIrna(1) = 0;
croprotein(1) = 0;
crorna(1) = 0;
time(1) = 0;    %s

%start the loop
for i = 1: numsteps-1
    dcIProtein_dt = ((omegacI * cIrna(i)) - (XcIprot*cIprotein(i)));
    dcIRNA_dt = ((mucI*(1-((croprotein(i)^2)/((kcroHalf^2) + (croprotein(i)^2))))) - (XcIrna*(cIrna(i))));
    dcroProtein_dt = ((omegacro * crorna(i)) - (Xcroprot*croprotein(i)));
    dcroRNA_dt = ((mucro*(1-((cIprotein(i)^2)/((kcIHalf^2) + (cIprotein(i)^2))))) - (Xcrorna*(crorna(i))));
    
    %increment and save with the d/dt values
    cIprotein(i+1) = cIprotein(i) + (dcIProtein_dt*delta_t);
    cIrna(i+1) = cIrna(i) + (dcIRNA_dt*delta_t);
    croprotein(i+1) = croprotein(i) + (dcroProtein_dt*delta_t);
    crorna(i+1) = crorna(i) + (dcroRNA_dt*delta_t);
    
    time(i+1) = time(i) + delta_t;
end    

figure;
plot(time,cIrna);
hold on
plot(time,cIprotein);
hold on
plot(time,crorna);
hold on
plot(time,croprotein);

title('Change in concentrations of RNA and protein over time')
xlabel('time (s)')
ylabel('rna or protein concentration (mM)')
legend('cIrna','cIprotein', 'crorna', 'croprotein')
hold off

%Second Simulation of Part 1 Question 1
%Initial values
cIprotein(1) = 0; %mM
cIrna(1) = 0;
croprotein(1) = 0;
crorna(1) = 20;
time(1) = 0;    %s

%start the loop
for i = 1: numsteps-1
    dcIProtein_dt = ((omegacI * cIrna(i)) - (XcIprot*cIprotein(i)));
    dcIRNA_dt = ((mucI*(1-((croprotein(i)^2)/((kcroHalf^2) + (croprotein(i)^2))))) - (XcIrna*(cIrna(i))));
    dcroProtein_dt = ((omegacro * crorna(i)) - (Xcroprot*croprotein(i)));
    dcroRNA_dt = ((mucro*(1-((cIprotein(i)^2)/((kcIHalf^2) + (cIprotein(i)^2))))) - (Xcrorna*(crorna(i))));
    
    %increment and save with the d/dt values
    cIprotein(i+1) = cIprotein(i) + (dcIProtein_dt*delta_t);
    cIrna(i+1) = cIrna(i) + (dcIRNA_dt*delta_t);
    croprotein(i+1) = croprotein(i) + (dcroProtein_dt*delta_t);
    crorna(i+1) = crorna(i) + (dcroRNA_dt*delta_t);
    
    time(i+1) = time(i) + delta_t;
end    

figure;
plot(time,cIrna);
hold on
plot(time,cIprotein);
hold on
plot(time,crorna);
hold on
plot(time,croprotein);

title('Change in concentrations of RNA and protein over time')
xlabel('time (s)')
ylabel('rna or protein concentration (mM)')
legend('cIrna','cIprotein', 'crorna', 'croprotein')
hold off

%Simulation three Part 1 Question 1

%Initial values
cIprotein(1) = 50; %mM
cIrna(1) = 0;
croprotein(1) = 0;
crorna(1) = 0;
time(1) = 0;    %s

%start the loop
for i = 1: numsteps-1
    dcIProtein_dt = ((omegacI * cIrna(i)) - (XcIprot*cIprotein(i)));
    dcIRNA_dt = ((mucI*(1-((croprotein(i)^2)/((kcroHalf^2) + (croprotein(i)^2))))) - (XcIrna*(cIrna(i))));
    dcroProtein_dt = ((omegacro * crorna(i)) - (Xcroprot*croprotein(i)));
    dcroRNA_dt = ((mucro*(1-((cIprotein(i)^2)/((kcIHalf^2) + (cIprotein(i)^2))))) - (Xcrorna*(crorna(i))));
    
    %increment and save with the d/dt values
    cIprotein(i+1) = cIprotein(i) + (dcIProtein_dt*delta_t);
    cIrna(i+1) = cIrna(i) + (dcIRNA_dt*delta_t);
    croprotein(i+1) = croprotein(i) + (dcroProtein_dt*delta_t);
    crorna(i+1) = crorna(i) + (dcroRNA_dt*delta_t);
    
    time(i+1) = time(i) + delta_t;
end    

figure;
plot(time,cIrna);
hold on
plot(time,cIprotein);
hold on
plot(time,crorna);
hold on
plot(time,croprotein);

title('Change in concentrations of RNA and protein over time')
xlabel('time (s)')
ylabel('rna or protein concentration (mM)')
legend('cIrna','cIprotein', 'crorna', 'croprotein')
hold off
