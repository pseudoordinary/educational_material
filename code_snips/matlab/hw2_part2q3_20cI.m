%Project 2 Part 2 Question 3 - 20 cI rna to start

%Initializing variables for rate constants and initial number of molecules
XcIrna = 1.2; %s^-1
XcIprot = 1.2; %s^-1

Xcrorna = 0.8; %s^-1 
Xcroprot = 0.8; %s^-1

omegacI = 50; %s^-1
mucI = 50; %s^-1
omegacro = 50; %s^-1myi
mucro = 50; %s^-1

kcIHalf = 10; %molecules/cell * s^-1
kcroHalf = 10; %molecules/cell * s^-1

initial_num = 0; %initial number of molecules
total_time = 1.0; %total time in seconds 
num_steps = 50000;

%Initial arrays
%arrays
cIprotein = zeros(num_steps,1);
cIrna = zeros(num_steps,1);
croprotein = zeros(num_steps,1);
crorna = zeros(num_steps,1);
time = zeros(num_steps, 1);

%Initial values
cIprotein(1) = 0; %mM
croprotein(1) = 0;
cIrna(1) = 20;
crorna(1) = 0;
time(1) = 0;                 % time array, first element

num_reacts = 8;
rxn_rate = zeros(num_reacts,1);
alpha0 = 0;
%while (time(i) < total_time)
for(i=1: num_steps)    
    %compute reaction rates
    for(m = 1:num_reacts)
        if(m == 1)
        rxn_rate(m) = ((omegacI * cIrna(i)));
        elseif(m == 2)
        rxn_rate(m) = ((XcIprot)* (cIprotein(i)));
        elseif(m == 3)
        rxn_rate(m) = (mucI * (1-((croprotein(i)^2)/((kcroHalf^2) + (croprotein(i)^2)))));
        elseif(m == 4)
        rxn_rate(m) = ((XcIrna) * (cIrna(i)));
        elseif(m == 5)
        rxn_rate(m) = ((omegacro) * crorna(i));
        elseif(m == 6)
        rxn_rate(m) = ((Xcroprot) * croprotein(i));
        elseif(m == 7)
        rxn_rate(m) = (mucro * (1-((cIprotein(i)^2)/((kcIHalf^2) + (cIprotein(i)^2)))));
        elseif(m == 8)
        rxn_rate(m) = ((Xcrorna) * (crorna(i)));   
        end
    end
    
    %choose the next reaction
    alpha0 = sum(rxn_rate);
    y = rand();     % y is drawn from U(0,1)
    y_mod = alpha0*y;   % y is drawn from U(0, alpha0)
    
    %find the time increment
    p = rand();              % y is drawn from U(0,1)
    tau = -log(p)/(alpha0);    % tau is drawn from Exp(alpha)
    
    next_rxn = 0;
    for(j = 1: num_reacts)
        if(y_mod < sum(rxn_rate(1:j)))
            next_rxn = j;
            break;
        end
    end
    
    %update number of molecules and time
    if (next_rxn == 1)
        %update numbers based on reaction 1, synthesis of cIprotein
        cIrna(i+1) = cIrna(i);
        crorna(i+1) = crorna(i);
        croprotein(i+1) = croprotein(i);
        cIprotein(i+1) = cIprotein(i) + 1; 
    elseif (next_rxn == 2)
        %update numbers based on reaction 2, degradation of cIprotein
        cIrna(i+1) = cIrna(i);
        crorna(i+1) = crorna(i);
        croprotein(i+1) = croprotein(i);
        cIprotein(i+1) = cIprotein(i) - 1;
    elseif (next_rxn == 3)
        %update numbers based on reaction 3, synthesis of cI rna
        cIprotein(i+1) = cIprotein(i);
        crorna(i+1) = crorna(i);
        croprotein(i+1) = croprotein(i);
        cIrna(i+1) = cIrna(i) + 1; 
    elseif (next_rxn == 4)
        %update numbers based on reaction 4, degradation of cI rna
        cIprotein(i+1) = cIprotein(i);
        crorna(i+1) = crorna(i);
        croprotein(i+1) = croprotein(i);
        cIrna(i+1) = cIrna(i) - 1; 
    elseif (next_rxn == 5)
        %update numbers based on reaction 5, synthesis of cro protein
        croprotein(i+1) = croprotein(i) + 1;
        cIprotein(i+1) = cIprotein(i);
        crorna(i+1) = crorna(i);
        cIrna(i+1) = cIrna(i);
    elseif (next_rxn == 6)
        %update numbers based on reaction 6, degradation of cro protein
        cIprotein(i+1) = cIprotein(i);
        crorna(i+1) = crorna(i);
        cIrna(i+1) = cIrna(i);
        croprotein(i+1) = croprotein(i) - 1;
    elseif (next_rxn == 7)
        %update numbers based on reaction 7, synthesis of cro rna
        crorna(i+1) = crorna(i) + 1;
        cIprotein(i+1) = cIprotein(i);
        cIrna(i+1) = cIrna(i);
        croprotein(i+1) = croprotein(i);
    elseif (next_rxn == 8)
        %update numbers based on reaction 8, degradation of cro rna
        cIprotein(i+1) = cIprotein(i);
        croprotein(i+1) = croprotein(i);
        cIrna(i+1) = cIrna(i);
        crorna(i+1) = crorna(i) - 1; 
    else
        % there is only eight reactions
        disp('something went wrong!');
    end
    
    %time for when the next reaction is happening
    time(i+1) = time(i)+tau; % increment time
    
end

figure;
hold on;
stairs(time,croprotein);
stairs(time,crorna);
stairs(time,cIprotein);
stairs(time,cIrna);
title('Stochastic change in concentrations of RNA and protein over time')
xlabel('time (s)')
ylabel('rna or protein concentration (mM)')
legend('croprotein', 'crorna','cIprotein','cIrna')
hold off