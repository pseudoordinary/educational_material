from copy import deepcopy
import time
import sys

# Open the file
# Read each line of the file and parse the numbers, place into instance variables
# Once you finish parsing, this set of instructions, close the file

num = -1;
cap = -1;
refill = -1;
booth_fun = []
total_coins_right_now = -1;


#ask the user for the input name of the file
file_to_open = "";

########################################################################################
def get_information(inputfile):
    #loop through the file for the information wanted
    for line in inputfile:
        #first line should be the number of the booths
        if line.__contains__("num"):
            # get the number after ( and before )
            global num
            first = line.split('(')
            second = first[1].split(')')
            num = int(second[0])
        elif line.__contains__("cap"):
            # get the cap of the coins
            global cap
            first = line.split('(')
            second = first[1].split(')')
            cap = int(second[0])
            global total_coins_right_now
            total_coins_right_now = int(second[0])
        elif line.__contains__("refill"):
            # get the refill amount
            global refill
            first = line.split('(')
            second = first[1].split(')')
            refill = int(second[0])
        elif line.__contains__("fun"):
            # get the fun and place it in the correct place in the array
            # index, value
            global booth_fun
            first = line.split('(')
            second = first[1].split(')')
            third = second[0].split(',')
            booth_fun.insert(int(third[0]), int(third[1]))
            # if there are multiple, it will overwrite the previous booth number
        #stop the parsing if it is the end of file or there is nothing on the line

#########################################################################################

def check_all_positives():
    global booth_fun
    for i in range(len(booth_fun)-1):
        if(i<num):
            if(booth_fun[i]<=0):
                return False
    return True

#########################################################################################

#consider zero a negative cause you dont want to waste coins on a zero booth
def check_all_negative():
    global booth_fun
    for i in range(len(booth_fun)-1):
        if(i<num):
            if(booth_fun[i]>0):
                return False
    return True

#########################################################################################

def there_exists_a_negative_but_not_entire_list():
    global booth_fun
    if ((check_all_positive == False) and (check_all_negative() == False)):
        #there is a mixture
        return True
    else:
        return False

#########################################################################################

def max_if_all_positives():
    #find the max value multiple that by the cap and multiple all the other values by the refill value
    global num
    global cap
    global refill
    global booth_fun
    #find the max fun value
    copied = deepcopy(booth_fun)
    copied.sort()
    #for the rest of the fun values, loop through the fun values and multiple by the refill value

    if(refill >=1):
        print "refill"
        print booth_fun
        total = (int(copied[num-1]) * cap) #this is just the max value
        print total
        print len(copied)
        for i in range(len(copied)-1):
            if(i < num):
                total = (total + (copied[i]*refill))
        print total
        return total
    else:
        #refill is zero, so you have to use one at all the booths and the rest on the largest
        total = (int(copied[num-1]) * (cap-(num-1)))
        for i in range(len(copied)-1):
            if(i < num):
                total = (total + (copied[i]*1))
        return total


#########################################################################################

def max_if_all_negative():
    #find the max value multiple that by the cap and multiple all the other values by the refill value
    global num
    global cap
    global refill
    global booth_fun
    #find the max fun value
    copied = deepcopy(booth_fun)
    copied.sort()
    if(refill >= 1):
        total = (int(copied[num-1]) * cap) #this is just the max value
        #for the rest of the fun values, loop through the fun values and multiple by the refill value
        for i in range(len(copied)-1):
            if(i < num):
                total = (total + (copied[i]*1))
        return total
    else:
        #refill is zero, so you have to use one at all the booths and the rest on the largest
        total = (int(copied[num-1]) * (cap-(num-1)))
        for i in range(len(copied)-1):
            if(i < num):
                total = (total + (copied[i]*1))
        return total

#########################################################################################

def find_max():
    global booth_fun
    global refill
    global total_coins_right_now
    total_max = 0
    index_of_highest = -1
    value_at_index = -1
    which_booth = 0
    start_range = 0
    coins_now = total_coins_right_now
    at_second_biggest = False
    while (which_booth <= num):
        value_at_index = -1;
        if(which_booth == num):
            return total_max
        else:
            #go to the highest value
            for i in range(start_range, len(booth_fun)):
                #reset value at index

                if(booth_fun[i]>value_at_index):
                    #if its greater the stored values
                    index_of_highest = i
                    value_at_index = booth_fun[i]
            #now you have the max value

            #iteriate until you reach the index of the highest value
            for i in range(start_range,index_of_highest+1):
                #negative or zero value
                if(booth_fun[i]<=0):
                    #use one coin
                    coins_now = coins_now-1
                    total_max = total_max + (booth_fun[i]*1)
                    coins_now = coins_now + refill
                    if(coins_now>cap):
                        coins_now = cap
                    which_booth = which_booth+1;
                    #booth_fun[i] = -1
                #positive value
                else:
                #if its not the largest value, use the refill amount of coins
                    if(i != index_of_highest):
                     #   print 'Not the largest value so the refill amount was the coins'

                        if((booth_fun[i]<booth_fun[i+1]) and (coins_now+refill > cap)):
                            #current + refill - cap, use this value for fill
                            total_max = total_max + ((coins_now+refill)-cap)*booth_fun[i]
                            coins_now = cap
                            which_booth = which_booth +1
                        elif((at_second_biggest == False) or (refill == 1) or (booth_fun[i]>booth_fun[i+1])):
                            coins_now = coins_now - refill
                            total_max = total_max + (booth_fun[i]*refill)
                            which_booth = which_booth + 1
                        else:
                        #start collecting for the next biggest
                            coins_now = coins_now - (refill-1)
                            total_max = total_max + (booth_fun[i]*(refill-1))
                            which_booth = which_booth + 1
                     #   booth_fun[i] = -1
                    else:
                    #once it reaches the biggest int, you dump all your coins on there

                        total_max = total_max + (booth_fun[index_of_highest]*coins_now)
                        coins_now = coins_now - coins_now
                        start_range = index_of_highest+1
                        at_second_biggest = True

                    #moving booths
                        which_booth = which_booth + 1
                    coins_now = coins_now + refill
                    if (coins_now > cap):
                        coins_now = cap
                   # print 'which booth at the end: '+str(which_booth)
                    #print 'coins_now: '+ str(coins_now)

                #the rest of the array, you need to keep going, so loop this until you reach the end

#########################################################################################
def main():
#open the input file
    inputfile = open(sys.argv[1],'r')
    get_information(inputfile)
    total_max = find_max()
    print "Total max fun: " + str(total_max)
    inputfile.close()

if __name__ == "__main__":
    main()