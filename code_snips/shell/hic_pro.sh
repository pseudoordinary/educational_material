#!/bin/bash

## Number of threads to use when mapping the reads
THREADS=5

## Temporary storage ( a new directory will be created in here )
## This override's SGE TMP directory which is automatically deleted by SGE
# using 'TMPDIR' will affect 'mktemp' behaviour
TMPDIR=/tmp
export TMPDIR

##
## HiC wrapper for glx
##

CONFIG_FILE="$1"
INPUT1="$2"
INPUT2="$3"
OUT_ZIP_LOG="$4" ##zip
OUT_BAM_BWT2="$5"
#OUT_MPAIRSTAT_BWT2="$6"
OUT_100000bed_HIC="$6" ##can compress/gzip this folder
OUT_500000bed_HIC="$7"
OUT_1000000_MATRIX="$8"
OUT_500000_MATRIX="$9"
BIN_SIZE="${10}"
shift 11

GENOME="dummy_var"

HIC_V="$(which HiC-Pro)"
R_V="$(which R)"

##
## Preprocess:
## Create a tmp input folder
TMPDIR=/tmp/hic
export TMPDIR
TMP_INPUT_TOP=$(mktemp -d -t glxhicproinput.XXXXXXXXX)
TMPDIR=$TMP_INPUT_TOP
export TMPDIR
TMP_INPUT=$(mktemp -d --tmpdir=$PWD -t glxhicproinput.XXXXXXXXX)
TMPDIR=/tmp
export TMPDIR
[ ! -z "$DEBUG" ] && echo "HIC-PRO INPUT directory = $OUTPUT_DIR"
chmod g+rwX "$TMP_INPUT" || exit 1
[ -e "$INPUT1" ] || { echo "Error: input file '$INPUT1' not found." >&2 ; exit 1 ; }
cp "$INPUT1" "$TMP_INPUT" || { echo "Error: failed to copy '$INPUT1' to '$TMP_INPUT'" >&2 ; exit 1 ; }
cp "$INPUT2" "$TMP_INPUT" || { echo "Error: failed to copy '$INPUT2' to '$TMP_INPUT'" >&2 ; exit 1 ; }

COUNTER=1
TMP_FILE_NAME="dummy_text"
SECOND_TO_LAST="dummy_text"

echo "If the output is a bam file, please change the datatype by pressing the pencil icon and going to the datatype tab. Thank you."
echo "$TMP_INPUT"

for f in "$TMP_INPUT"/*.dat

do
	if [ "$COUNTER" == "1" ]; then
		TMP_FILE_NAME=$f
		SECOND_TO_LAST=$(basename $(dirname $TMP_FILE_NAME))
		mv -vf "$f" "${f%.dat}"_R"$COUNTER".fastq;
	fi

	if [ "$COUNTER" == "2" ]; then
                #this means you are looking at the next file, can you change the file name?
		mv -vf "$f" "$TMP_FILE_NAME"
		mv -vf "$TMP_FILE_NAME" "${TMP_FILE_NAME%.dat}"_R"$COUNTER".fastq;
        fi


	let COUNTER=$COUNTER+1; 
done

##for file in "$TMP_INPUT"/*.fastq; do gzip "$file"; done


##
## Step 0:
## Find the Genome specific Config file location
if [ "$CONFIG_FILE" == "hg19-hindiii" ] && [ "$BIN_SIZE" == "1000000" ]

then

	GENOME="/export/hic-config/config_hg19.txt"
fi

if [ "$CONFIG_FILE" == "hg19-hindiii" ] && [ "$BIN_SIZE" == "20000" ]

then

        GENOME="/export/hic-config/config_hg19_20k.txt"
fi

if [ "$CONFIG_FILE" == "hg19-hindiii" ] && [ "$BIN_SIZE" == "40000" ]

then

        GENOME="/export/hic-config/config_hg19_40k.txt"
fi

if [ "$CONFIG_FILE" == "hg19-hindiii" ] && [ "$BIN_SIZE" == "150000" ]

then

        GENOME="/export/hic-config/config_hg19_150k.txt"
fi



if [ "$CONFIG_FILE" == "hg19-mboi" ] && [ "$BIN_SIZE" == "1000000" ]

then

        GENOME="/export/hic-config/config_mboi_hg19.txt"
fi

if [ "$CONFIG_FILE" == "hg19-mboi" ] && [ "$BIN_SIZE" == "20000" ]

then

        GENOME="/export/hic-config/config_mboi_hg19_20k.txt"
fi

if [ "$CONFIG_FILE" == "hg19-mboi" ] && [ "$BIN_SIZE" == "40000" ]

then

        GENOME="/export/hic-config/config_mboi_hg19_40k.txt"
fi

if [ "$CONFIG_FILE" == "hg19-mboi" ] && [ "$BIN_SIZE" == "150000" ]

then

        GENOME="/export/hic-config/config_mboi_hg19_150k.txt"
fi



if [ "$CONFIG_FILE" == "mm9-hindiii" ] && [ "$BIN_SIZE" == "1000000" ]

then

        GENOME="/export/hic-config/config_mm9.txt"
fi

if [ "$CONFIG_FILE" == "mm9-hindiii" ] && [ "$BIN_SIZE" == "20000" ]

then

        GENOME="/export/hic-config/config_mm9_20k.txt"
fi

if [ "$CONFIG_FILE" == "mm9-hindiii" ] && [ "$BIN_SIZE" == "40000" ]

then

        GENOME="/export/hic-config/config_mm9_40k.txt"
fi

if [ "$CONFIG_FILE" == "mm9-hindiii" ] && [ "$BIN_SIZE" == "150000" ]

then

        GENOME="/export/hic-config/config_mm9_150k.txt"
fi




if [ "$CONFIG_FILE" == "mm9-mboi" ] && [ "$BIN_SIZE" == "1000000" ]

then

        GENOME="/export/hic-config/config_mboi_mm9.txt"
fi

if [ "$CONFIG_FILE" == "mm9-mboi" ] && [ "$BIN_SIZE" == "20000" ]

then

        GENOME="/export/hic-config/config_mboi_mm9_20k.txt"
fi

if [ "$CONFIG_FILE" == "mm9-mboi" ] && [ "$BIN_SIZE" == "40000" ]

then

        GENOME="/export/hic-config/config_mboi_mm9_40k.txt"
fi

if [ "$CONFIG_FILE" == "mm9-mboi" ] && [ "$BIN_SIZE" == "150000" ]

then
        GENOME="/export/hic-config/config_mboi_mm9_150k.txt"
fi


##
## Step 1:
## Run HIC-Pro, store results in a temporary directory
OUTPUT_DIR=$(mktemp -d -t glxhicpro.XXXXXXXXX)
[ ! -z "$DEBUG" ] && echo "HIC-PRO Output directory = $OUTPUT_DIR"
chmod g+rwX "$OUTPUT_DIR" || exit 1

[ -e "$TMP_INPUT" ] || { echo "Error: input directory '$TMP_INPUT' not found." >&2 ; exit 1 ; }

TMPFILE="$OUTPUT_DIR/hic_pro.stderr.txt"
[ ! -z "$DEBUG" ] && { echo "HiC-Pro STDERR = $TMPFILE" ; }

/galaxy_venv/bin/HiC-Pro_2.9.0/bin/HiC-Pro -i "$TMP_INPUT_TOP" -o "$OUTPUT_DIR" -c "$GENOME"  2> $TMPFILE >&2
EXITCODE=$?
if [ "$EXITCODE" -ne "0" ]; then
        echo "Error: hic-pro failed." >&2
        echo "Command line was:" >&2
        echo "HiC-Pro -i $TMP_INPUT_TOP -o "$OUTPUT_DIR" -c $GENOME  2> $TMPFILE" >&2
        echo >&2
        echo "Error was:" >&2
        cat "$TMPFILE" >&2
        echo "HiC-Pro Output directory = $OUTPUT_DIR"
        exit $EXITCODE
fi

##
## Step 2
##   Copy HiC-Pro's output files from OUTPUT_DIR to glx's output files
OUT_MAPPING_PIC="$13"
CONFIG_FILE="$14"

SAVE_LOCATION=$PWD
cd $OUTPUT_DIR
echo $OUTPUT_DIR
#copy the picture files into the log file
cp -r hic_results/pic logs
cp -r hic_results/matrix logs
cp -r hic_results/data logs
##Compress the log folder
tar -czvf "logs_and_pics.tar.gz" "logs"
export PWD=$SAVE_LOCATION

cp "$OUTPUT_DIR/logs_and_pics.tar.gz" "$OUT_ZIP_LOG" || { echo "Error: failed to copy '$OUTPUT_DIR/logs_and_pics.tar.gz' to '$OUT_ZIP_LOG'" >&2 ; exit 1 ; }

files=( $OUTPUT_DIR/bowtie_results/bwt2/$SECOND_TO_LAST/*.bwt2pairs.bam )

sed 1d "${files[0]}" > "$OUT_BAM_BWT2" || { echo "Error: failed to transfer '$OUTPUT_DIR/bowtie_results/bwt2/input/*.bwt2pairs.bam' to '$OUT_BAM_BWT2'" >&2 ; exit 1 ; }

#files=( $OUTPUT_DIR/bowtie_results/bwt2/$SECOND_TO_LAST/*.mpairstat )

#sed 1d "${files[0]}" > "$OUT_MPAIRSTAT_BWT2" || { echo "Error: failed to transfer '$OUTPUT_DIR/bowtie_results/bwt2/input/input.mpairstat' to '$OUT_MPAIRSTAT_BWT2'" >&2 ; exit 1 ; }

#cp $OUTPUT_DIR/bowtie_results/bwt2/$SECOND_TO_LAST/*.mpairstat $OUTPUT_DIR/hic_results/data

#SAVE_LOCATION=$PWD
#cd $OUTPUT_DIR/hic_results
##Compress the log folder
#tar -czvf "data.tar.gz" "data"
#export PWD=$SAVE_LOCATION

#bed file for 1000000
cp "$OUTPUT_DIR/hic_results/matrix/$SECOND_TO_LAST/raw/$BIN_SIZE/"$SECOND_TO_LAST"_"$BIN_SIZE"_ord.bed" "$OUT_100000bed_HIC" || { echo "Error: failed to copy '$OUTPUT_DIR/hic_results/data.tar.gz' to '$OUT_100000bed_HIC'" >&2 ; exit 1 ; }

#bed file for 5000000
cp "$OUTPUT_DIR/hic_results/matrix/$SECOND_TO_LAST/raw/500000/"$SECOND_TO_LAST"_500000_ord.bed" "$OUT_500000bed_HIC" || { echo "Error: failed to copy '$OUTPUT_DIR/hic_results/pic.tar.gz' to '$OUT_500000bed_HIC'" >&2 ; exit 1 ; }

#SAVE_LOCATION=$PWD
#cd $OUTPUT_DIR/hic_results
##Compress the log folder
#tar -czvf "pic.tar.gz" "pic"
#export PWD=$SAVE_LOCATION

#cp "$OUTPUT_DIR/hic_results/pic.tar.gz" "$OUT_PDF_PIC" || { echo "Error: failed to copy '$OUTPUT_DIR/hic_results/pic.tar.gz' to '$OUT_PDF_PIC'" >&2 ; exit 1 ; }

sed 1d "$OUTPUT_DIR/hic_results/matrix/$SECOND_TO_LAST/iced/$BIN_SIZE/"$SECOND_TO_LAST"_"$BIN_SIZE"_iced.matrix" > "$OUT_1000000_MATRIX" || { echo "Error: failed to transfer '$OUTPUT_DIR/hic_results/matrix/input/iced/$BIN_SIZE/input_"$BIN_SIZE"_iced.matrix' to '$OUT_1000000_MATRIX'" >&2 ; exit 1 ; }

sed 1d "$OUTPUT_DIR/hic_results/matrix/$SECOND_TO_LAST/iced/500000/"$SECOND_TO_LAST"_500000_iced.matrix" > "$OUT_500000_MATRIX" || { echo "Error: failed to transfer '$OUTPUT_DIR/hic_results/matrix/input/iced/500000/input_500000_iced.matrix' to '$OUT_500000_MATRIX'" >&2 ; exit 1 ; }

rm -rf $OUTPUT_DIR && rm -rf $TMP_INPUT && rm -rf $TMP_INPUT_TOP
#[ ! -z "$DEBUG" ] && rm -rf $OUTPUT_DIR && rm -rf $TMP_INPUT
