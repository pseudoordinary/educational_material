#ifndef __HASHSTUFF_H__
#define __HASHSTUFF_H__

#include <openssl/rand.h>
#include <openssl/sha.h>
#include <openssl/err.h>

unsigned int generate_salt(void);

#endif