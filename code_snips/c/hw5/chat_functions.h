#ifndef __CHAT_FUNCTIONS_H__
#define __CHAT_FUNCTIONS_H__


/* mandated in the document */
#define MAX_MSG_LENGTH 1001
/* I made this number up it can be changed accordingly */
#define MAX_USR_NAME_LENGTH 100
#define PORT 4567
#define SUCCESS 0
#define FAILURE -1
#define EPOLL_EVENT_TOTAL 10
 
/* Made up change later */
#define MAX_USERS 100

/* Does what it looks like, identifies the type of message sent*/
enum message_type
{
	client_msg,
	server_msg,
	client_to_client,
};

/* Keeping this shit contained no potato code. */
struct registered_users
{
	int file_descriptors[MAX_USERS];
	int that_thing;
}; 

/* struct to keep all the user message information contained */
struct usr_message
{
	int type;
	char message[MAX_MSG_LENGTH];
	char name[MAX_USR_NAME_LENGTH];
};

#endif