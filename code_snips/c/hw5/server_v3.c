#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <sys/epoll.h>
#include "server.h"
#include "chat_functions.h"
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include "database_interface.h"
#include <setjmp.h>
#include <time.h>

jmp_buf place;

const char *delimiter = " ";

/* Client list */
//static some_node* client_list;

static struct connected_clients* current_client_list = NULL;
 

/* Going to hold our properly connected clients. */
static server_chat_rooms_t* room_list = NULL;

static struct database_connection* connected_database = NULL;

/* Our thread pool. Static so nobody messes with it. */
static thread_pool_t* pool = NULL;
user_db_info* client_info = NULL;

server_cmd_args* pointer_struct = NULL;
int listening_on;
int max_rooms = 5;

void sigpipe_handler()
{
    //die.id = pthread_self();
    longjmp(place, 1);
}

void sigint_handler()
{
    printf("GOT INTO THE SIGINT HANDLER YEEA \n");
    shutdown(listening_on, SHUT_RD);
    pthread_mutex_lock(&(pool->queue_lock));

    thread_worker_t* current = pool->queue_head;
    thread_params_t* params;

    while(current)
    {
        params = (thread_params_t*)current->arg;
        //shutdown(*(params->accept_fd), SHUT_RDWR);
        close(*(params->accept_fd));
        if(current->next)
            current = current->next;
        else
            break;  
    }   

    int i = 0;
    for (; i < 10; i++)
    { 
        pthread_kill(pool->thread_id[i], SIGINT);
    }


    /*TODO SEND BYE TO CLIENTS*/


    pthread_mutex_unlock(&(pool->queue_lock));
    pthread_cond_signal(&(pool->queue_ready));
    
    printf("DO YOU MAKE IT HERE BDDY \n");
    shutdown(listening_on, SHUT_WR);
    printf("ABOUT TO CLOSE \n");
    close(listening_on);
    exit(EXIT_SUCCESS);
}

int remove_user_by_fd(int client_fd)
{   
    pthread_mutex_lock(&current_client_list->array_lock);
    printf("AFTER LOCK\n");
    int found = 0;
    int i = 0;

    for(; i < 1000; i++)
    {
        printf("THE VALUE OF NAME IN THE LOOP: %s\n", current_client_list->connected_users[i].name);
        printf("IN FOR LOOP\n");
        if(current_client_list->connected_users[i].accept_fd == client_fd)
        {        
            current_client_list->connected_users[i].client_id = 0;
            current_client_list->connected_users[i].accept_fd = 0;
            current_client_list->connected_users[i].current_room = -1;
            current_client_list->connected_users[i].session_id = -1;
            current_client_list->connected_users[i].still_connected = -1;
            current_client_list->connected_users[i].new_user_flag = 0;

            current_client_list->connected_users[i].status_flag = 0;
            if(current_client_list->connected_users[i].name != NULL)
                free(current_client_list->connected_users[i].name);
   
            found = 1;
            break;
        }
        if(found == 1)
            break;
    }

    while(found == 0 || i == 10000)
    {
      
        printf("GOT HERE WWAITING FOR THE USER LIST. \n");
        pthread_cond_wait(&(current_client_list->array_ready), &(current_client_list->array_lock));                    
        printf("AFTER THE WAIT \n");
    }
    
    pthread_cond_signal(&(current_client_list->array_ready));
    pthread_mutex_unlock(&(current_client_list->array_lock));

    return 0;
}

int init_connection()
{
		struct sockaddr_in server;
        int listen_fd;
        if((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
        	return -1;      
        }
             
        server.sin_family = AF_INET;
        server.sin_port = htons(pointer_struct->port_num);
        server.sin_addr.s_addr = htonl(INADDR_ANY);

        int optval = 1;

        if(setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) <0)
        {
            perror("Set socket option failed.");
        }

        if(setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) <0)
        {
            perror("Set socket option failed.");
        }
        
        /* Give ourselves a local name for the fd and listen for connections. 
           The 5 in the listen call is for the max listen queue. We should
           adjust.  */
        if(bind(listen_fd, (struct sockaddr *)&server, sizeof(server)) < 0)
        {
        	return -1;
        }
        listen(listen_fd, 5);

        

        printf("server start listening on port 4567. \r\n");
        return listen_fd;
}

int set_nonblocking(int listen_fd)
{
    // struct flock some_flock;
    
    // fcntl(listen_fd, F_GETLK, F_GETLK);
    // fcntl(listen_fd, F_SETLKW, &some_flock);

	/*  If data is not available when you call read, then the system call will fail with a return value of -1 and errno is set to EAGAIN. */
	if(fcntl(listen_fd, F_SETFL, fcntl(listen_fd, F_GETFD, 0) | O_NONBLOCK) == -1)
    {
    	 return -1;
    }

    return 0;
}


int register_pool_worker(void *(*process) (void *arg, char* buf), void *arg)
{
	/* Creates a new worker with the specified function. */ 
    thread_worker_t* worker = (thread_worker_t*) malloc(sizeof(thread_worker_t));
    worker->process = process;
    worker->arg = arg;
    worker->next = NULL;
    
    /* lock the resource. */
    pthread_mutex_lock (&(pool->queue_lock));
    thread_worker_t* temp = pool->queue_head;

    /* Find a spot in the list. */
    if(temp != NULL)
    {
        while(temp->next != NULL)
        {
        	temp = temp->next;
        }

        temp->next = worker;
    }
    else
    {
        pool->queue_head = worker;
    }

    if(pool->queue_head == NULL)
    	exit(EXIT_FAILURE);

    pool->current_queue_size++;
        printf("Current queue size: %d\r\n", pool->current_queue_size);
    
    /* Unlock the mutex and set the queue to ready. */
    pthread_mutex_unlock(&(pool->queue_lock));
    pthread_cond_signal(&(pool->queue_ready));
    
    return 0;
}

int alternate_read(int accept_fd, char* buf1)
{
	char* buffer = (char*) malloc(3000);	
	char* position = buffer;
	ssize_t bytes_read = 0;
	int total = 0;

    memset(buf1, '\0', 1001);

    char* temp_buff = malloc(500);

	while(bytes_read < 3000)
	{
		printf("number of bytes total: %d\n" ,total);

		//18,446,744,073,709,551,615
		
	rev_restart:	
		if((bytes_read = recv(accept_fd, temp_buff, 500, 0)))
		{
			position += bytes_read;
			total += bytes_read;

			printf("number of bytes read: %zu\n" ,bytes_read);
			printf("number of total read: %d\n" ,total);

            strcat(buffer, temp_buff);
            memset(temp_buff,'\0',500);

		}
        else if(bytes_read == 0)
        {
            close(accept_fd);
        }
		else
		{
            if(errno == ETIMEDOUT || errno == ECONNRESET || errno == 107)
            {
              printf("The connection to the server has been lost.\n Please restart the application and specify a valid PIRC server.\n" );
              //shutdown(accept_fd, SHUT_RDWR);
              close(accept_fd);
            }
            else if(errno == EINTR)
            {
              printf("EINTR \n" );
              goto rev_restart;
            }

			perror("Error on read client socket");
			shutdown(accept_fd, SHUT_RDWR);
		}

		if(total < 0)
		{
			printf("NOT ENOUGH BULLSHIT \n");
			return -1;
		}
		printf("BEFORE THE QUESTIONABLE CHECK \n" );
		char temps = *(buffer + total);
		if(temps == '\0' || temps == '\000' || strstr(buffer, "\r\n") != NULL)
		{
			printf("FOUND THE END OF STRING \n" );
			break;
		}

		printf("what the duce?\n");
	}

	strcpy(buf1, buffer);


return total;
}

void* some_process(void *arg, char *buf)
{
	int receive_bytes;
	char buf1[1001];
    memset(buf1,'\0', 1001);
	struct param *param;
	param = (thread_params_t*) arg;
	printf("Starting process ...\r\n");

	UNUSED(buf);
	int fuck_you_compiler;
    
  //  printf("DID YOU GET HERE \n");
    while(1)
    {
        //receive_bytes = read_rec(*(param->accept_fd), buf1, sizeof(buf1));
        receive_bytes = alternate_read(*(param->accept_fd), buf1);


        printf("THE NUMBER OF BYTES READ %d\n", receive_bytes);
        if(receive_bytes == -1)
        {
    //    	printf("WHATEVER YOU WANT I DON'T CARE PEANUT BUTTER AND JELLY\n");
            if(errno == EAGAIN)
            {
            	break;    
            }       
            break;
        }
        else if(receive_bytes == 0)
        {
            epoll_ctl(*(param->epoll_fd), EPOLL_CTL_DEL, *(param->accept_fd), param->ev);    
            
            fuck_you_compiler = *(param->maxfd)--;
            UNUSED(fuck_you_compiler);
            break;         
        }               
        else
        {
	        //buf1[receive_bytes] = '\0';
	        printf("The client sent this information. %s\r\n", buf1);
	        printf ("threadid is 0x %x\n", (unsigned int) pthread_self());
	        int recv_check = check_recieved_message(buf1, *(param->accept_fd), param);

            if(recv_check == -3)
            {
                //then do stuff
                close(*(param->accept_fd));
                return 0;
            }

	        //send(*(param->accept_fd), buf1, sizeof(buf1) , 0);

        }
        if(setjmp(place) != 0)
        {
      //      printf("I HAVE NO IDEA HOW TO USE LONG JUMPS \n");
        //    printf("DID THINGS I SHOULD NOT HAVE \n");
            //shutdown(*(param->accept_fd), SHUT_RDWR);
            close(*(param->accept_fd));
            pthread_exit(NULL);
        }
       
    }                           
    return NULL;
    shutdown(*(param->accept_fd), SHUT_RDWR);
}

/* Needed to wrap the appropriate steps. */
void* run_thread(void* arg)
{
    //signal(SQUIGGLESQUIGGLE, squiggle_squiggle_handler);
	UNUSED(arg);
    printf ("starting thread 0x%x\n",(unsigned int) pthread_self ());
    while (1)
    {
    	/* lock the resource */
        pthread_mutex_lock (&(pool->queue_lock));
        
        while(pool->current_queue_size == 0 && !pool->shutdown)
        {
            printf("thread 0x%x is waiting\n", (unsigned int) pthread_self());
            pthread_cond_wait(&(pool->queue_ready), &(pool->queue_lock));
        }
       
        printf("thread 0x%x is starting to work\n", (unsigned int) pthread_self());
       
        if(pool->current_queue_size == 0)
        	exit(EXIT_FAILURE);
        if(pool->queue_head == NULL)
        	exit(EXIT_FAILURE);
        
        /* Grap the worker from the queue and run the process. */
        pool->current_queue_size--;
        thread_worker_t* worker = pool->queue_head;
        pool->queue_head = worker->next;

        /* Unlock the resource. */
        pthread_mutex_unlock(&(pool->queue_lock));
        printf("start process...\r\n"); 
        (*(worker->process)) (worker->arg, worker->buf);
        
        free(worker);
        
        worker = NULL;
    }
    /* Exit the thread safely. */
   // `it (NULL);
}


int accept_clients(int listen_fd)
{       
	int epoll_fd;   
	int maxfd;
	int epoll_events_ready;
	int accept_fd;
	int i;
	struct sockaddr_in client;
	struct epoll_event ev;
	struct epoll_event events[15];
	unsigned int length = sizeof(client);       
	//char buffer[1001];

    signal(SIGPIPE | EPIPE, sigpipe_handler);

	/* Need our listening socket to be nonblocking. */
	if(set_nonblocking(listen_fd) < 0)
	{
	        printf("fcntl error\r\n");
	        return -1;
	} 

	epoll_fd = epoll_create(15);
	if(epoll_fd == 0)
	{
	        printf("create error\r\n");     
	        return -1;
	}

	/* Look for level and edge triggers. */
	ev.events = EPOLLIN | EPOLLET;
	ev.data.fd = listen_fd;

	if(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listen_fd, &ev) < 0)
	{
	        printf("epoll add error\r\n");
	        return -1;
	}
	else
	{
	        printf("listen id %d\r\n",(int)listen_fd);
	}       
	//set_keep_alive(listen_fd);
	maxfd = 1;
	while(1)
	{
	        epoll_events_ready = epoll_wait(epoll_fd, events, maxfd, -1);

	        if(epoll_events_ready == -1)
	        {
	                printf("wait\r\n");
	                return -1;      
	        }

	        for(i = 0; i < epoll_events_ready ; i++)
	        {
                if((events[i].events & EPOLLERR )|| (events[i].events & EPOLLHUP))
                {
                    perror("");
                    printf("There is some error occuring with epoll. :( \n");
                    close(events[i].data.fd);
                }
                else if(events[i].data.fd == listen_fd)
                {
            		/* Accept the connection. */ 
                    accept_fd = accept(listen_fd, (struct sockaddr *)&client, &length);
                    if(accept_fd < 0)
                    {
                            printf("accept error\r\n");
                            continue;
                    }
        
                    /* Makes sure that the fd we have a connection on doesn't block. */
                    set_nonblocking(accept_fd);
                    ev.events = EPOLLIN | EPOLLET;
                    ev.data.fd = accept_fd;

                    //set_socket_keep_alive(&accept_fd);

                    if(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, accept_fd, &ev) < 0)
                    {
                            printf("epoll add error\r\n");
                            return -1;
                    }
                    else
                    {
                        printf("new client %d\r\n", (int)accept_fd);
                    	maxfd++;
                    }       
                }       
                else if(events[i].events&EPOLLIN)
                {
                    thread_params_t* params = (thread_params_t*) malloc (sizeof (thread_params_t));
                    params->epoll_fd = &epoll_fd;
                    params->accept_fd = &ev.data.fd;       
                    params->ev = &ev;
                    params->maxfd = &maxfd;
                    params->ptr = current_client_list;
                    
                    register_pool_worker(some_process, params);
                }
	        }
	}
	close(listen_fd);
	return 0;
}


void init_thread_pool(int max_thread_num)
{
	pool = (thread_pool_t*) malloc(sizeof(thread_pool_t));

	/* Initialize the mutex and the cond. The mutex allows
	   for "mutual exclusion" and our cond will allow us
	   to wait for a specific state. */
    pthread_mutex_init (&(pool->queue_lock), NULL);
    pthread_cond_init (&(pool->queue_ready), NULL);
    
    pool->queue_head = NULL;
    pool->max_thread_num = max_thread_num;
    pool->current_queue_size = 0;
    pool->shutdown = 0;
    pool->thread_id = (pthread_t *)malloc (max_thread_num * sizeof (pthread_t));
    
    /* Populate the pool. */
    int i = 0;
    for (i = 0; i < max_thread_num; i++)
    { 
    	/* Bind each thread. */
        pthread_create(&(pool->thread_id[i]), NULL, run_thread, NULL);
    }

}


/* 
void send_to_clients(char* out_message)
{
	pthread_mutex_lock(&lock);

	struct usr_message message;

	message.type = server_msg;
	strncpy(message.message, out_message, sizeof(message.message));

	int i;	
	for(i = 0; i < client.that_thing; i++)
	{
		send(client.file_descriptors[i], &message, sizeof(message), 0);
	}

	pthread_mutex_unlock(&lock);
}

*/

/* 
void send_to_specific_client(char* out_message, int client)
{
	pthread_mutex_lock(&lock);


	printf("SEND MESSAGE TO CLIENT: %s", out_message);
	printf("THIS IS THE SIZE OF THE ACK: %zu\n", strlen(out_message) );

	send(client, out_message, strlen(out_message) * sizeof(char) + 1, 0);

	pthread_mutex_unlock(&lock);
}
*/

void register_user(char* message, int client_socket, int status)
{
    printf("TOP OF REGISTER USER\n");
    pthread_mutex_lock(&current_client_list->array_lock);
    printf("AFTER LOCK\n");
    int found = 0;
    int i = 0;

    UNUSED(message);
    for(; i < 1000; i++)
    {
        printf("THE VALUE OF NAME IN THE LOOP: %s\n", current_client_list->connected_users[i].name);
        printf("IN FOR LOOP\n");
        if(current_client_list->connected_users[i].status_flag == 0)
        {
            printf("THE VALUE OF NAME: %d\n", current_client_list->connected_users[i].status_flag);
            printf("FOUND A MATCH\n");
            printf("THE VALUE OF THE COUNTER IS %d\n", i);
            current_client_list->connected_users[i].accept_fd = client_socket;
            current_client_list->connected_users[i].name = (char*)malloc(strlen(message)+1);
            printf("THE VALUE OF NAME: %s\n", current_client_list->connected_users[i].name);
            printf("TH LENGTH OF THE NAME: %zu\n", strlen(message)+1);

            strncpy(current_client_list->connected_users[i].name, message, strlen(message)+1);
            printf("THE VALUE OF NAME AFTER STRNCPY: %s\n", current_client_list->connected_users[i].name);
            printf("STRNCPY SUCCESSFUL\n");
             
             if(status == USER_NOT_IN_DB)
                current_client_list->connected_users[i].status_flag = 1;
            else
                current_client_list->connected_users[i].status_flag = status;
            
            found = 1;
            break;
        }
        if(found == 1)
            break;
    }           

    while(found == 0)
    {
      
        printf("GOT HERE WWAITING FOR THE USER LIST. \n");
        pthread_cond_wait(&(current_client_list->array_ready), &(current_client_list->array_lock));                    
        printf("AFTER THE WAIT \n");
    }
    
    pthread_cond_signal(&(current_client_list->array_ready));
    pthread_mutex_unlock(&(current_client_list->array_lock));

    //current_client_list->connected_users->status_flag = 0;

    printf("BEFORE THE RETURN IN REGISTER_USER %s\n", current_client_list->connected_users[i].name );
}


int opt_server(int argc, char* argv[])
{

printf("hello get opt\n");

    server_cmd_args keep_server_args;
    char buffer_motd [5000];

    memset(buffer_motd, '\0', 5000);

    int opt_returned = 0;
    //parse the command line arguments
    // /server [-he] [-N num] PORT_NUMBER MOTD

    /*check for the wrong number of arguments*/
    
    if(argc < 3)
    {
        if(argc == 2)
        {
            if(strcmp(argv[1],  "-h") != 0)
            {
                printf("There are the wrong number of arguments\n");
                return -1;
            }
            else
            {
                HELP_SERVER(argv[0]);
                exit(EXIT_SUCCESS);     
            }
        }
        printf("There are the wrong number of arguments\n");
        return -1;
    }

    printf("argc: %d\n", argc);
    /*look for input arguments*/
    while((opt_returned = getopt(argc, argv, "heN:")) != EOF)
    {
        printf("opt_returned: %d\n", opt_returned);

        switch(opt_returned)
        {
            case 'h': 
                //help and return successllo get opt
                printf("GOT IN \n" );
                //help(SERVER);
                HELP_SERVER(argv[0]);
                exit(EXIT_SUCCESS);
                break;
            case 'e':
                //echo messages recieved on stdout of server
                keep_server_args.echo_flag = 1;
                break;
            case 'N':
                //specifies the name num of chatrooms allowed on the server, default is five
                max_rooms = atoi(optarg);
                break;
                //check
                if(argc < 5)
                {
                    printf("There are the wrong number of arguments\n");
                    return -1;
                }

                
        }
    }

    if(optind < argc)
    {
        //set the server number
        int port_num = atol(argv[optind]);

        int check = check_if_port_alright(port_num);

        if(check == -1)
        {
            printf("You supplied a port which is not in the valid range of 1024 - 65535\n");
            exit(EXIT_FAILURE);
        }

        keep_server_args.port_num = port_num;

        // plus one for port num
        int counter = optind + 1; 
        while(counter < argc)
        {
            //loop through for the values
            snprintf(buffer_motd, strlen(argv[counter]), "%s", argv[counter]);
            snprintf(buffer_motd, 1, " ");
            counter++;
        }
        buffer_motd[counter] = '\r';
        buffer_motd[counter+1] = '\n';
        keep_server_args.motd = buffer_motd;

    }
    else
    {
        //you goofed
        printf("You messed up buddy.");
    }

    pointer_struct = &keep_server_args;

return 0;

}


int main(int argc, char ** argv, char **envp)
{

    printf("THINGS AND SUCH SUCH AND THINGS \n");
    signal(SIGINT, sigint_handler);
    int check = opt_server(argc, argv);

    if(check == -1)
    {
        /* CHANGE IF TIME ALLOWS */
        printf("You did not follow the specified usage. \n Please re-run with correct arument list. Look at the help menu below.\n");
        exit(EXIT_FAILURE);
    }

   
    printf("STEP ONE: INIT THE DATABSE CONNECTION \n");    
    struct database_connection sql_database; 
    int db_check = init_db_connection(&sql_database);


    struct connected_clients some_user_data; 
    printf("STEP TWO: INIT CLIENT LIST \n");
    

    server_chat_rooms_t rooms;
    init_room_data_structure(&rooms); 

    room_list = &rooms;

    int client_list_check = init_client_data_structure(&some_user_data);
    current_client_list = &some_user_data;

    if(db_check == -1 || client_list_check != 0)
        printf("THE DATABASE HAS NOT BEEN SUCCESSFULLY CONNECTED TO \n");
    //else
      //  printf("Connection successful in main. \n");

    printf("STEP ONE SUCCESSFULL: DATABASE CONNECTION INIT \n");
    printf("STEP TWO SUCCESSFULL: CLIENT LIST INIT \n");

    connected_database = &sql_database;


    /* File descriptor that we are going to listen on if everything goes well. */
    init_thread_pool(10);
    listening_on = init_connection();

    /* Got those connections yo. */
    accept_clients(listening_on);  
    
    printf("%d\n", argc );
	printf("%s\n", *argv );
	printf("%s\n", *envp );

    return EXIT_SUCCESS;	
} 


void send_to_specific_client(char* out_message, int client)
{
	printf("SEND MESSAGE TO Client: %s", out_message);
	printf("THIS IS THE SIZE OF THE ACK: %zu\n", strlen(out_message) );
	//while(1);

    UNUSED(out_message);
    UNUSED(client);

    send(client, out_message, (strlen(out_message) + 1) * sizeof(char), 0);
    printf("AFTER ACK? \n");
}

int find_registered_user_by_name(char* name)
{
    printf("In the find_registered_user_by_name function.\n");

    int client_check;

    printf("Before the lock\n");
    pthread_mutex_lock(&(current_client_list->array_lock));
    printf("I am locked.\n");
    int stop = 0;
    int i = 0;
    //int found = -1;
    int status = -1;
    printf("Before the for loop.\n");
    for(; i < 1000; i++)
    {
        if(current_client_list->connected_users[i].status_flag > 0 && current_client_list->connected_users[i].status_flag < 50)
        {
            printf("Icurrent_client_list->connected_users[i].status_flag %d\n", current_client_list->connected_users[i].status_flag);
        
            if(strcmp(current_client_list->connected_users[i].name, name) == 0)
            {

                 printf("Icurrent_client_list->connected_users[i].name %s\n", current_client_list->connected_users[i].name);
        
                client_check = 1;
                stop = 1;
      //          found = i;

                status = current_client_list->connected_users[i].status_flag;
            }
            if (stop == 1)
                break;  
        }
    }

    if((i + 1) == 1000)
        client_check = -1;

    while(client_check == 0)
    {
      
        printf("WAITING FOR THE USER LIST AGAIN. \n");
        pthread_cond_wait(&(current_client_list->array_ready), &(current_client_list->array_lock));                    
        printf("AFTER THE WAIT \n");
    }

    pthread_mutex_unlock(&(current_client_list->array_lock));
    pthread_cond_signal(&(current_client_list->array_ready));

    return status;
}



/* This function returns the index of the user with the associated file descriptor
   in the connected_client_list. Returns -1 if not found.*/  
 int find_registered_user(int client_fd)
 {
    int client_check;
    pthread_mutex_lock(&(current_client_list->array_lock));
    int stop = 0;
    int i = 0;
    int found = -1;

    for(; i < 10000; i++)
    {
            if(current_client_list->connected_users[i].accept_fd == client_fd)
            {
                client_check = 1;
                stop = 1;
                found = i;
            }
            if (stop == 1)
                break;  
    }

    if((i + 1) == 10000)
        client_check = -1;

    while(client_check == 0)
    {
      
        printf("STILL THE USER LIST APPARANTLY. \n");
        pthread_cond_wait(&(current_client_list->array_ready), &(current_client_list->array_lock));                    
        printf("AFTER THE WAIT \n");
    }

    pthread_mutex_unlock(&(current_client_list->array_lock));
    pthread_cond_signal(&(current_client_list->array_ready));
    return found;
 }






/*check the message that was recieved and do the  appropriate action*/
int check_recieved_message(char *recieved_message, int client_fd, thread_params_t* param)
{
	
    printf("THIS IS THE VALUE OF THE LIST IN THE PARAM STRUCT: %p\n", param->ptr );
    printf("MADE IT INTO CHECK_RECEIVED_MESSAGE \n");
    //printf("hello there recieved_message: %sistherespace\n", recieved_message);
	/* tokenize */

    char* cmd_cpy = strdup(recieved_message);

    //token contains the rest of the string
    //recieved_mes,sage contains the token

	int length = strlen(recieved_message); 
	char new_line = '\n';  
	char carriage_ret = '\r';
	
	char new_line_check = recieved_message[length - 1];
	char carriage_ret_check = recieved_message[length - 2];
	
	if(new_line_check != new_line  || carriage_ret != carriage_ret_check)
	{
		char* error = create_error_message(100, NULL);
		send_to_specific_client(error, client_fd);
	}

    //newline_remove(recieved_message);

    /*tokenize*/
    char *recieved_tokens[512];
    char *tokenized[512];

    int null_count = 0;
    while(null_count < 512)
    {
      recieved_tokens[null_count] = NULL;
      tokenized[null_count] = NULL;
      null_count++;
    }

    //char** temp = tokenized;
    char* some_token;
    //printf(" COPY: %s\n", cmd_cpy);

    char* pointer = NULL;
    pointer  = cmd_cpy;
    printf("POinter before: %s\n", pointer);
    some_token = strsep(&pointer, " ");
    //printf("sometoken: %s\n" ,some_token);
    printf("POinter after: %s\n", pointer);

    if(some_token != NULL)
        tokenized[0] = some_token;
    int i = 1;

    printf("token[0]: %s\n",tokenized[0]);
    while(pointer != NULL)
    {
        some_token = strsep(&pointer, " ");
        tokenized[i] = some_token;
        
        printf("some Token: %s\n", some_token);
        printf("Tokenized i: %s\n", tokenized[i]);
        i++;
    }

    if(pointer == NULL)
    {

        /* DO SOMETHING HERE */
    }

    UNUSED(recieved_tokens);
    UNUSED(tokenized);
   // UNUSED(counter);

	//char *token_name;
   //token_name = strtok(recieved_message, " ");

    const void* aloha_cmd = "ALOHA!";
    const void* iam_cmd = "IAM";
    const void* msg_cmd = "MSG";
    const void* iamnew_cmd = "IAMNEW";
    const void* creater_cmd = "CREATER";
    const void* listr_cmd = "LISTR";
    const void* kick_cmd = "KICK";
    const void* join_cmd = "JOIN";
    const void* leave_cmd = "LEAVE";
    const void* tell_cmd = "TELL";
    const void* listu_cmd = "LISTU";
    const void* bye_cmd = "BYE \r\n";

   
    //void* ahola_cmd = "AHOLA!";
    // void* hinew_cmd = "HINEW ";
     void* new_pass_cmd = "NEWPASS";
    // void* rtsil_cmd = "RTSIL";
    // void* nioj_cmd = "NIOJ";
    // void* evael_cmd = "EVAEL";
    // void* llet_cmd = "LLET";
    // void* echop_cmd = "ECHOP";
    // void* utsil_cmd = "UTSIL";
    //void* auth_cmd = "AUTH";
    void* pass_cmd = "PASS";

    //remember to free bruh/
    //cmd checking

  //  printf("cmd_cpy: %sISTHERESOMESTUFFHERE?\n" , cmd_cpy);
    //printf("bye_cmd: %shello??????????????\n" , (char*)bye_cmd);

    	if(strcmp(tokenized[0], aloha_cmd) == 0)
    	{
    		printf("GOT THROUGH THE COMPARISON \n");
				//
				//char* ack = create_ack_message("!AHOLA \r\n", NULL, NULL);
				//printf("THIS IS THE ACK MESSAGE: %s ghuiop\n", ack);
				send_to_specific_client("!AHOLA \r\n", client_fd);
				//register_user("dummy", client_fd);
				
			
				printf("DEAR GOD PLEASE REGISTER A USER. \n");
				/* TODO SEND REVERSE ALOHA! */
			

    		//make a message
                free(cmd_cpy);

    		//send the message back
    	}
        else if (strcmp(tokenized[0], pass_cmd) == 0)
        {
            printf("FUCK YOU ID YOU GOT INTO THE PASSWORD \n");
            if(i != 3 || validate_pass(tokenized[1]) == 1)
            {
                char* bad_pass_error = create_error_message(61, NULL);
                send_to_specific_client(bad_pass_error, client_fd);
                return -6;
            }   
            else
            {
                int client_check = find_registered_user(client_fd);
                user_db_info con_client;
                int flag = 0;
        
                //get_client_data(connected_database->database, client);
                if(client_check == -1)
                {
                   char* error_message_100 = create_error_message(100, "Information was lost in account creation.");
                   send_to_specific_client(error_message_100, client_fd);
                   send_to_specific_client("BYE \r\n", client_fd);

                   /* TODO MAKE PROPER DISCONNECTION FOR USER. 
                      REMOVE FROM CONNECTED USER LIST, SET USED FLAG TO 0
                      FREE CHAR* NAME, CLOSE FILE DESCRIPTOR 
                    */
                      return -6;
                }

               pthread_mutex_lock(&(current_client_list->array_lock));
               //printf("THE VALUE OS THE NAME FOR THE CLIENT FOR FUCKS SAKE PLEASE WORK: %s\n", current_client_list->connected_users[i].name);
               con_client.username = current_client_list->connected_users[client_check].name;               
               flag = current_client_list->connected_users[client_check].status_flag;
                   /* WHERE WE REPLACE THE SALT AND THE PASWORD WITH THE HASHED VERSION */     
               pthread_mutex_unlock(&(current_client_list->array_lock));
               pthread_cond_signal(&(current_client_list->array_ready));

               if(flag != 2)
               {
                    char* error_message_100 = create_error_message(100, "Login process did not go through correctly.");
                    send_to_specific_client(error_message_100, client_fd);
                    send_to_specific_client("BYE \r\n", client_fd);

                    /*close the file descriptor*/
                    /*set the dudes use flag to zero.*/
                    return -6;
               }

               pthread_mutex_lock(&connected_database->list_lock);


               int in_database = 0;
                //in_database = query_user(connected_database, tokenized[1]);
                
                in_database = get_client_data(connected_database->database,  &con_client);

                while(in_database == 0)
                {
                    printf("GOT HERE TYRING TO GRAB THE CLIENT DATA.\n");
                    pthread_cond_wait(&(connected_database->list_ready), &(connected_database->list_lock));                    
                    printf("AFTER THE WAIT \n");
                }
                
                pthread_cond_signal(&(connected_database->list_ready));
                pthread_mutex_unlock(&connected_database->list_lock);

                printf("THIS IS THE RETURNED PASSWORD %sSPACE?\n", con_client.password );
                printf("THIS IS WHAT WE WILL COMPARE AGAINST %sSPACE?\n\n\n", tokenized[1] );
                newline_remove(tokenized[1]);
                printf("THIS IS WHAT WE WILL COMPARE WITH THE NEW LINE REMOVED: %sSPACE\n", tokenized[1]);
                if(strcmp(tokenized[1], con_client.password) != 0)
                {
                    char * error_number_61 = create_error_message(61, NULL);
                    send_to_specific_client(error_number_61, client_fd);
                    send_to_specific_client("BYE \r\n", client_fd);

                    /* TODO close the file descriptor here*/

                    return -6;
                }

               pthread_mutex_lock(&(current_client_list->array_lock));
               //printf("THE VALUE OS THE NAME FOR THE CLIENT FOR FUCKS SAKE PLEASE WORK: %s\n", current_client_list->connected_users[i].name);
               current_client_list->connected_users[client_check].status_flag = 3;               
               pthread_mutex_unlock(&(current_client_list->array_lock));
               pthread_cond_signal(&(current_client_list->array_ready));
            
                

                char* hi_ack = create_ack_message("HI", con_client.username, NULL);
                
                send_to_specific_client(hi_ack, client_fd);

                current_client_list->connected_users[i].status_flag = USER_LOGGED_IN;
                printf("current_client_list->connected_users[i].status_flag %d\n", current_client_list->connected_users[i].status_flag);




            }
        }
        else if(strcmp(tokenized[0], new_pass_cmd) == 0)
        {
            if(i != 3 || validate_pass(tokenized[1]) == 1)
            {
                char* bad_pass_error = create_error_message(61, NULL);
                send_to_specific_client(bad_pass_error, client_fd);
                return -6;
            }   
            else
            {
                printf("ARE YOU GETTING IN HERE? \n");
                int client_check = find_registered_user(client_fd);
                user_db_info con_client;
                //pthread_mutex_lock(&current_client_list->list_lock);
                int flag = 0;


                if(client_check == -1)
                {
                   char* error_message_100 = create_error_message(100, "Information was lost in account creation.");
                   send_to_specific_client(error_message_100, client_fd);
                   send_to_specific_client("BYE \r\n", client_fd);

                   /* TODO MAKE PROPER DISCONNECTION FOR USER. 
                      REMOVE FROM CONNECTED USER LIST, SET USED FLAG TO 0
                      FREE CHAR* NAME, CLOSE FILE DESCRIPTOR 
                    */
                      return -6;
                }

               pthread_mutex_lock(&(current_client_list->array_lock));
               printf("THE VALUE OS THE NAME FOR THE CLIENT FOR FUCKS SAKE PLEASE WORK: %s\n", current_client_list->connected_users[client_check].name);
               con_client.username = current_client_list->connected_users[client_check].name;               
               flag = current_client_list->connected_users[client_check].status_flag;
                   /* WHERE WE REPLACE THE SALT AND THE PASWORD WITH THE HASHED VERSION */     
                pthread_mutex_unlock(&(current_client_list->array_lock));
                pthread_cond_signal(&(current_client_list->array_ready));

                con_client.password = tokenized[1];
               con_client.salt = 0;

                if(flag == -1)
                {
                   char* error_message_100 = create_error_message(100, "Information was lost in account creation.");
                   send_to_specific_client(error_message_100, client_fd);
                   send_to_specific_client("BYE \r\n", client_fd);

                   /* TODO MAKE PROPER DISCONNECTION FOR USER. 
                      REMOVE FROM CONNECTED USER LIST, SET USED FLAG TO 0
                      FREE CHAR* NAME, CLOSE FILE DESCRIPTOR 
                    */
                      return -6;
                }
 
                printf("DO YOU GET IN HERE IN ORDER TO SOMETHING: \n");
                pthread_mutex_lock(&connected_database->list_lock);
                int in_database = 0;
                //in_database = query_user(connected_database, tokenized[1]);
                
                in_database = insert_user_to_db(connected_database->database,  &con_client);

                while(in_database == 0)
                {
                    printf(" GOT INTO THE DATABAE FOR INSERT\n");
                    pthread_cond_wait(&(connected_database->list_ready), &(connected_database->list_lock));                    
                    printf("AFTER THE WAIT \n");
                }
                
                pthread_cond_signal(&(connected_database->list_ready));
                pthread_mutex_unlock(&connected_database->list_lock);

                char* hi_ack = create_ack_message("HI", con_client.username, NULL);
                
                send_to_specific_client(hi_ack, client_fd);

                current_client_list->connected_users[i].status_flag = USER_LOGGED_IN;
                printf("current_client_list->connected_users[i].status_flag %d\n", current_client_list->connected_users[i].status_flag);

            /* TODO ECHO MOTD */
            }
        }
        else if(strcmp(recieved_message, bye_cmd) == 0)
        {
            printf("HELLO THERE, I'M IN BYE \n");
            send_to_specific_client("BYE \r\n", client_fd);
            remove_user_by_fd(client_fd);
        }
    	else if(strcmp(tokenized[0], iam_cmd) == 0)
    	{
            printf("MADE IT INTO THE IAM FUNCTION \n");
            if(i != 3)
            {
                /* MUST SEND BACK ERROR 100 INTERNAL SERVER ERROR AS THE CLIENT
                   DID NOT SEND BACK THE PROPER STRING */
                char* error_100 = create_error_message(100, "Client did not send back the proper string.");
                send_to_specific_client(error_100, client_fd);
            }
            else
            {
                printf("ABOUT TO CHECK THE GLOBAL STRUCTURE\n");
                printf("Tokenized[0] beginning %s\n", tokenized[1]);
                int status = find_registered_user_by_name(tokenized[1]);
                //int status = find_registered_user(client_fd);
                //int status = 0;
                printf("Tokenized[0] HEY CHECK THIS HERE THINGY %s\n", tokenized[1]);
                if(status == 3)
                {
                    char* error_message_00 = create_error_message(00, NULL);
                    send_to_specific_client( error_message_00 , client_fd);
                    send_to_specific_client("BYE \r\n", client_fd);

                    /*do the thing and close the file descriptor*/
                    /* Don't remove user from list. */
                }
        
                printf("RETURNED FROM CHECK THE GLOBAL STRUCTURE.\n");
                printf("BEFORE DB QUERY \n");
                pthread_mutex_lock(&connected_database->list_lock);
                
                int in_database = 0;

                in_database = query_user(connected_database->database, tokenized[1]);
                printf("DOES IT RETURN FROM QUERY USER? \n");

                while(in_database == 0)
                {
                    printf("GOT HERE TRYING TO QUERY THE USER\n");
                    pthread_cond_wait(&(connected_database->list_ready), &(connected_database->list_lock));    
                    printf("AFTER THE WAIT \n");
                }

                pthread_cond_signal(&(connected_database->list_ready));
                pthread_mutex_unlock(&connected_database->list_lock);

                printf("AFTER DB QUERY \n");
                if(in_database == -1)
                {
                    char* error_message_02 = create_error_message(02, NULL);
                    send_to_specific_client(error_message_02, client_fd);
                    send_to_specific_client("BYE \r\n", client_fd);
                    /* TODO CLOSE CONNECTION */
                }
                else
                {
                    char* auth_message = create_ack_message("AUTH",tokenized[1] , NULL);
                    send_to_specific_client(auth_message, client_fd);
                    register_user(tokenized[1], client_fd, 2);

                    printf("--------------------------------------------finding: %s\n", tokenized[1]);
                    find_registered_user_by_name(tokenized[1]);

                    printf("--------------------------------------------AUTH was made into. :D ");
                }
            }
    	}
    	else if(strcmp(tokenized[0], msg_cmd) == 0)
    	{
    		//client trying to send a private message

    	}
    	else if(strcmp(tokenized[0], iamnew_cmd) == 0)
    	{
            printf("THE VALUE OF THE COUNTER I: %d\n", (i -1));
            if(i != 3)
            {
                char * error_message_100 = create_error_message(100, "Client did not send in the proper string.");
                send_to_specific_client(error_message_100, client_fd);
                send_to_specific_client("BYE \r\n", client_fd);

                /*clsoe off the file description*/
                /* MUST SEND BACK ERROR 100 INTERNAL SERVER ERROR AS THE CLIENT
                   DID NOT SEND BACK THE PROPER STRING */
            }
            else
            {

                printf("Got into the else statement within the IAMNEW block--------\n");
                pthread_mutex_lock(&connected_database->list_lock);
                int in_database = 0;
                in_database = query_user(connected_database->database, tokenized[1]);
                printf("Before the while loop.\n");
                while(in_database == 0)
                {
                    printf("GOT HERE TRYING TO QUERED THE USER.\n");
                    pthread_cond_wait(&(connected_database->list_ready), &(connected_database->list_lock));
                    
                    
                    printf("AFTER THE WAIT \n");
                }
                
                pthread_cond_signal(&(connected_database->list_ready));
                pthread_mutex_unlock(&connected_database->list_lock);
                // -1 is not in the database
                if(in_database == -1)
                {
                    
                    printf("ATTEMPTING TO REGISTER A NODE \n" );
                    register_user(tokenized[1],*(param->accept_fd), USER_NOT_IN_DB);
                    //printf("NODE INFO%s \n", ((client_t*)current_client_list->client_list)->name);
                    //send hinew WHATUP

                    printf("THIS IS THE VALUE OF the name: %s\n", tokenized[1]);
                    char* hinew_ack = create_ack_message("HINEW", tokenized[1], NULL);
                    printf("THE VALUE OF THE ACK MESSAGE: %s\n", hinew_ack );
                    
                    send_to_specific_client(hinew_ack, *(param->accept_fd));

                    //going to set the status flag to 
                }
                //in the database
                else
                {
                    printf("BEFORE SENDIG THE MESSAGE TO THE CLIENT \n");
                    char* err_message = create_error_message(1, tokenized[1]);
                    char* ack_bye = create_ack_message("BYE", NULL, NULL);
                
                    printf("err message: %s\n", err_message);
                    printf("ack_bye: %s\n", ack_bye);

                    send_to_specific_client(err_message, client_fd);
                    printf("AFTER THE FIRST SEND \n");
                    
                    struct timespec time;
                    time.tv_sec = 3;
                    time.tv_nsec = 0;

                    nanosleep(&time, NULL);
                    printf("AFTER SLEEP \n");
   
                    send_to_specific_client(ack_bye, client_fd);

                    return -3;
                    //  while(1){}
                    printf("AFTER SECOND SEND \n");
                    printf("MADE IT HERE AFTER WE FOUND THE RECORD. \n");
                    //close   
                }
            }
    		//register a new username
            //spawn a new thread and then log the user in.
            printf("IAMNEW Comparison reached \n");

    	}
    	else if(strcmp(tokenized[0], creater_cmd) == 0)
    	{
    		//attempt to make a new room
            char* creater_ack = create_ack_message("RETAERC", tokenized[1], NULL);
            send_to_specific_client(creater_ack, client_fd); 

            create_room(tokenized[1], client_fd);

    	}
    	else if(strcmp(tokenized[0], listr_cmd) == 0)
    	{
    		//wants a list of all the rooms with name and ids
            char* creater_ack = create_ack_message("RTSIL", tokenized[1], NULL);
            send_to_specific_client(creater_ack, client_fd); 
            
            list_all_rooms(client_fd);
    	}
    	else if(strcmp(tokenized[0], kick_cmd) == 0)
    	{
    		//user wanted to kick someone out of a room
            char* creater_ack = create_ack_message("KCIK", tokenized[1], NULL);
            send_to_specific_client(creater_ack, client_fd); 

            kick_user_from_room( tokenized[1] , client_fd);
    	}
    	else if(strcmp(tokenized[0], join_cmd) == 0)
    	{
    		//user wants to join the room, take care of this
            char* creater_ack = create_ack_message("NIOJ", tokenized[1], NULL);
            send_to_specific_client(creater_ack, client_fd); 

            join_a_room(atoi(tokenized[1]), client_fd);

    	}
    	else if(strcmp(tokenized[0], leave_cmd) == 0)
    	{
    		//user leaves chat and goes to the waiting room

            char* creater_ack = create_ack_message("EVAEL", NULL, NULL);
            send_to_specific_client(creater_ack, client_fd); 
    	
            leave_a_room(client_fd);

        }
    	else if(strcmp(tokenized[0], tell_cmd) == 0)
    	{
    		// tell command

    	}
    	else if(strcmp(tokenized[0], listu_cmd) == 0)
    	{
    		//list of users
            char* creater_ack = create_ack_message("RTSIL", tokenized[1], NULL);
            send_to_specific_client(creater_ack, client_fd); 

            list_users_in_room(client_fd);
    	}
    	else
    	{
    		//goof up?
    		
    	}


	return 0;
}

/*create error message*/
//ERR 30 <message> \r\n

char* create_error_message(int error_number, char * name)
{
	const void* err_msg = "ERR ";
    const void* carriage_return_newline;
    if(name != NULL){
        carriage_return_newline = " \r\n";    
    }
    else
    {
        carriage_return_newline = "\r\n";
    }
	
	const void* sorry_msg = " SORRY ";
	const void* user_exist_msg = " USER EXISTS ";
	const void* does_not_exist_msg = " DOES NOT EXIST ";
	const void* room_exist_msg = " ROOM EXISTS ";
	const void* maximum_rooms_reached_msg = " MAXIMUM ROOMS REACHED ";
	const void* room_does_not_exist_msg = " ROOM DOES NOT EXIST ";
	const void* user_not_present_msg = " USER NOT PRESENT ";
	const void* not_owner_msg = " NOT OWNER ";
	const void* invalid_use_msg = " INVALID USE ";
	const void* invalid_operation_msg = " INVALID OPERATION ";
	const void* invalid_password_msg = " INVALID PASSWORD ";
	const void* internal_server_error_msg = " INTERNAL SERVER ERROR ";
	const void* you_messed_up = " YOU MESSED UP ";

	/*REMEMBER TO FREE BRUH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1*/
    char* return_message = malloc(2000);
    //char return_message[5000];
    strcpy(return_message, err_msg);

    char buffer[8] = " ";
	int place = error_number;

	sprintf(buffer, "%d", place);

	/*append the error code*/
	strcat(return_message, buffer);

	if(error_number == 00)
	{
		//00 SORRY 
  		/*append the message*/
  		strcat(return_message, sorry_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
  		strcat(return_message, carriage_return_newline);
  		return return_message;

	}
	else if(error_number == 01)
	{
		//01 USER EXISTS 
		strcat(return_message, user_exist_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
		
	}
	else if(error_number == 02)
	{
		//02 DOES NOT EXIST 
		strcat(return_message, does_not_exist_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 10)
	{
		//10 ROOM EXISTS 
		strcat(return_message, room_exist_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 11)
	{
		//11 MAXIMUM ROOMS REACHED 
		strcat(return_message, maximum_rooms_reached_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 20)
	{
		//20 ROOM DOES NOT EXIST 
		strcat(return_message, room_does_not_exist_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 30)
	{
		//30 USERNOT PRESENT
		strcat(return_message, user_not_present_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;

	}
	else if(error_number == 40)
	{
		//40 NOT OWNER 
		strcat(return_message, not_owner_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 41)
	{
		//41 INVALID USE
		strcat(return_message, invalid_use_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 60)
	{
		//60 INVALID OPERATION 
		strcat(return_message, invalid_operation_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 61)
	{
		//61 INVALID PASSWORD 
		strcat(return_message, invalid_password_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 100)
	{
		//The default error message for any other conditions which were not mentioned:
    	//ERR 100 INTERNAL SERVER ERROR
    	strcat(return_message, internal_server_error_msg);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
    	strcat(return_message, carriage_return_newline);
    	return return_message;
	}
	else
	{
		// error message number was bad
		strcat(return_message, you_messed_up);
        if(name != NULL)
        {
            strcat(return_message, name);
        }
		strcat(return_message, carriage_return_newline);
		return return_message;

	}
}

int create_room(char* room_name, int client_fd)
{
    printf("Created a room\n");
    client_t* the_guy = get_user_info(client_fd);
    int using_rooms = 0;
    int found_spot = 0;
    

    pthread_mutex_lock(&room_list->array_lock);
    printf("Mutex Lock after in the create room function\n");
    if(room_list->num_in_use + 1 > max_rooms)
    {
        printf("In the if statement of the create room function\n");
        /* SEND ERROR HERE FOR MAX NUMBER OF ROOMS ALREADY */
        char* error_message_11 = create_error_message(11, "Max number of rooms was reached.");
        send_to_specific_client(error_message_11, client_fd);

        using_rooms = 1;
    }

    /* WALK THE LIST */

    int i = 0;
    printf("Before the for loop of the print room function\n");
    for(; i < max_rooms; i++)
    {
        if(room_list->rooms_inside[i].is_used != 0)
        {
            printf("COUNTING HOW MANY ROOMS THERE ARE: %d\n", i);
            if(strcmp(room_list->rooms_inside[i].room_name, room_name) == 0)
            {
                printf("In the if statement\n");
                /* ERROR GOES HERE ROOM WITH THAT NAME ALREADY EXISTS */
                char* error_message_10 = create_error_message(10, "Information was lost in account creation.");
                send_to_specific_client(error_message_10, client_fd);

                using_rooms = 1;
                break;
            }

            printf("This is before the found spot\n");
            if(found_spot == 0 && room_list->rooms_inside[i].is_used == 0)
            {
                found_spot = i;
            }


        }    
    }

    /* This means we walked the entire list and didn't find one with the previous name
        we are going to populate the room at the location of the first open position that
        we found before. */
        printf("Printing out i: %d\n", i);
    if(i >= max_rooms)
    {
        room_list->rooms_inside[found_spot].room_name = (char*)malloc((strlen(room_name) + 1) * sizeof(char));
        printf("Does this work?");
        strcpy(room_list->rooms_inside[found_spot].room_name, room_name);
        room_list->rooms_inside[found_spot].owner_fd = the_guy->accept_fd;
        room_list->rooms_inside[found_spot].owner_name  = (char*)malloc((strlen(the_guy->name) + 1) * sizeof(char));
        strcpy(room_list->rooms_inside[found_spot].owner_name, the_guy->name);
        room_list->rooms_inside[found_spot].id = room_list->next_available;
        room_list->next_available++; 
        room_list->rooms_inside[found_spot].is_used = 1;
        room_list->num_in_use++;
        using_rooms = 1;
    }

    printf("\n\n\n\n\nCreated room: \n");
    printf("THIS IS THE NAME %s\n", room_list->rooms_inside[found_spot].room_name );
    printf("THE OWNER IS %s\n", room_list->rooms_inside[found_spot].owner_name  );
    printf("THE OWNER FD iS %d\n\n\n\n\n",room_list->rooms_inside[found_spot].owner_fd );
    


    while(using_rooms == 0)
    {
        printf("GOT HERE TRYING TO QUERED THE USER.\n");
        pthread_cond_wait(&(room_list->array_ready), &(room_list->array_lock));  
        printf("AFTER THE WAIT \n");
    }
    
    pthread_cond_signal(&(room_list->array_ready));
    pthread_mutex_unlock(&room_list->array_lock);


    /* NOW WE MAKE THE USER JOIN HIS OWN ROOM MWAHAHAHAHAHA */
    /* NEEDED TO MAKE SURE WE RELEASE THE RESOURCES */
    printf("BEFORE MAKING THE USER JOIN ITS OWN ROOM\n");
    join_a_room(room_list->rooms_inside[i].id, the_guy->accept_fd);


    if(found_spot == 0)
        return -1;

    return 0;
}





int list_users_in_room(int client_fd)
{
    //malloc happened - remember to free bruh.
    char* create_send_message = malloc(5000);
    memset(create_send_message, '\0', 5000);
    const void* verb = "UTSIL ";
    const void* space = " ";
    const void* return_newline = "/r/n";
    const void* two_return_newlines = "/r/n/r/n";
    strcpy(create_send_message, verb);

    client_t* the_guy = get_user_info(client_fd);
    if(the_guy == NULL)
    {
        /* PROBABLY SEND INTERNAL SERVER ERROR
           AND BYE. */
        char* error_message_100 = create_error_message(100, "Cannot find user information.");
        send_to_specific_client(error_message_100, client_fd);
        char* bye_ack = create_ack_message("BYE /r/n", NULL, NULL);
        send_to_specific_client(bye_ack, client_fd);
    }

    int using_rooms = 0;
    int current_room = the_guy->current_room;
    if(current_room == -10)
    {
        /* YOU ARE IN THE WAITING ROOM
           THE DOCUMENT MAKES IT SEEM AS THOUGH
           WE CANNOT QUERY A LIST OF USERS IN THE
           WAITING ROOM */

        char* error_message_60 = create_error_message(60, "Cannot query list of users while in the waiting room.");
        send_to_specific_client(error_message_60, client_fd);      
    }


    pthread_mutex_lock(&room_list->array_lock);
    int i = 0;
    int count = room_list->rooms_inside[i].num_people;

    int found = 0;
    for(; i < 1000; i++)
    {
        /* FOUND THE ROOM */
        if(room_list->rooms_inside[i].id == current_room)
        {
            found = 1;
            /* Walk the users in the room */
            int j = 0;
            for(; j < 50; j++)
            {
                if(count > 0)
                {
                    /* IF THIS IS NOT THE LAST PERSON WE SEND \r\n at the end */
                    if(room_list->rooms_inside[i].client_list[j] != NULL)
                    {

                        /* NEED TO CREATE SENDING MESSAGE FOR USERNAME
                           room_list->rooms_inside[i].client_list[j].username */
                        strcat(create_send_message, (const char*)room_list->rooms_inside[i].client_list[j]);
                        strcat(create_send_message, space);
                        strcat(create_send_message, return_newline);

                        count--;

                    }
                }
                else
                {
                    /* NEED TO SEND \r\n\r\n since this SHOULD BE THE LAST PERSON
                       IN THE LIST. */
                    if(room_list->rooms_inside[i].client_list[j] != NULL)
                    {

                        /* NEED TO CREATE SENDING MESSAGE FOR USERNAME
                           room_list->rooms_inside[i].client_list[j].username */
                        strcat(create_send_message, (const char*)room_list->rooms_inside[i].client_list[j]);
                        strcat(create_send_message, space);
                        strcat(create_send_message, two_return_newlines);

                        /*send to the client after the message is appended*/
                        send_to_specific_client(create_send_message, client_fd);

                        count--;

                    }
                    using_rooms = 1;
                }

            }
            if(j >= 50)
            {
                /* ERROR GOES HERE NOBODY ELSE IN ROOM */

                char* error_message_30 = create_error_message(30, "There are no other users in this room.");
                send_to_specific_client(error_message_30, client_fd);
                using_rooms = 1;
            }
            if(found == 1)
            {
                using_rooms = 1;
                break;
            }
        }

        if(found != 0)
        {
            using_rooms = 1;
            break;
        }
    }


    while(using_rooms == 0)
    {
        printf("GOT HERE TRYING TO QUERED THE USER.\n");
        pthread_cond_wait(&(room_list->array_ready), &(room_list->array_lock));  
        printf("AFTER THE WAIT \n");
    }
    
    pthread_cond_signal(&(room_list->array_ready));
    pthread_mutex_unlock(&room_list->array_lock);

    return 0;
}


int kick_user_from_room(char* target, int owner_fd)
{
    client_t* potential_owner = get_user_info(owner_fd);
    if(potential_owner == NULL)
        return -1;

    pthread_mutex_lock(&room_list->array_lock);
    int using_rooms = 0;

    int i = 0;
    int j = 0;
    int which_room = -5;
    for(; i < max_rooms; i++)
    {
        if(strcmp(room_list->rooms_inside[i].owner_name, potential_owner->name) == 0)
        {
            which_room = i;
            break;
        } 
    }
    if(which_room < 0)
    {
        /* NEED TO SEND ERR 40 FAILED REQUEST; not owner */
        char* error_message_40 = create_error_message(40, "Not the owner of the room.");
        send_to_specific_client(error_message_40, owner_fd);
        using_rooms =1;
        return -1;

    }

    int target_found = 0;
    for(; j < 50; j++)
    {
        if(room_list->rooms_inside[which_room].client_list[j] != NULL)
        {
            if(strcmp(room_list->rooms_inside[which_room].client_list[j]->name, target) == 0)
            {
                room_list->rooms_inside[which_room].client_list[j] = NULL;

                /* NEED TO SEND KBYE TO THE TARGET FILE DESCRIPTOR 
                   IN potential_owner->accept_fd */
                char* kbye_ack = create_ack_message("KBYE /r/n", NULL, NULL);
                send_to_specific_client(kbye_ack, potential_owner->accept_fd);

                /* NEED TO ECHO THAT THE USER HAS BEEN KICKED OUT */
                potential_owner->current_room = -10;
                room_list->rooms_inside[which_room].num_people--;

                /* I THINK TECHNICALLY WE ARE SUPPOSED TO ECHO THAT THE NEW USER HAS
                   ENTERED THE WAITING ROOM HERE */
                target_found = 1;
                using_rooms = 1;
                break;
            }
        }

    }

    if(target_found != 1)
    {
        /* NEED TO SEND ERROR MESSAGE 41 INVALID USERNAME */
        char* error_message_41 = create_error_message(41, "Cannot find user to remove.");
        send_to_specific_client(error_message_41, owner_fd);
        
        using_rooms = 1;
    }

    while(using_rooms == 0)
    {
        printf("GOT HERE TRYING TO QUERED THE USER.\n");
        pthread_cond_wait(&(room_list->array_ready), &(room_list->array_lock));  
        printf("AFTER THE WAIT \n");
    }
    
    pthread_cond_signal(&(room_list->array_ready));
    pthread_mutex_unlock(&room_list->array_lock);

    return 1;
}



client_t* get_user_info(int client_fd)
{
    pthread_mutex_lock(&current_client_list->array_lock);
    printf("AFTER LOCK\n");
    int found = 0;
    int i = 0;
    int index  = 0;

    for(; i < 1000; i++)
    {
        if(current_client_list->connected_users[i].accept_fd == client_fd)
        {        

            found = 1;
            index = i;
            break;
        }
        if(found == 1)
            break;
    }

    while(found == 0 || i == 1000)
    {
      
        printf("GOT HERE WWAITING FOR THE USER LIST. \n");
        pthread_cond_wait(&(current_client_list->array_ready), &(current_client_list->array_lock));                    
        printf("AFTER THE WAIT \n");
    }
    
    pthread_cond_signal(&(current_client_list->array_ready));
    pthread_mutex_unlock(&(current_client_list->array_lock));

    if(found == 1)
        return &current_client_list->connected_users[index];;

    return NULL;
} 

/* This is gross but it should cover all cases. */

int leave_a_room(int client_fd)
{
    client_t* current_ = get_user_info(client_fd);
    /* THIS MEANS THAT THE USER IS IN THE WAITING ROOM
       AND WE DON'T HAVE TO DO ANYTHING, THE SPECIFIED
       BEHAVIOR IS ALREADY TO MOVE TO THE WAITING ROOM */


       if(current_ == NULL)
        {
            /* NEED TO DO SOMETHING HERE THINGKING JUST LETTING IT FALL THROUGH */
            return -1;
        }

    int room_id = current_->current_room;
    if( room_id == -10)
    {
        return -1;
    }

    pthread_mutex_lock(&room_list->array_lock);
    int using_rooms = 0;
    int i = 0;
    for(; i < max_rooms; i++)
    {
        if(room_list->rooms_inside[i].id == room_id && room_list->rooms_inside[i].is_used > 0)
        {
            /*NEED TO WALK THE USER LIST, FIND THE PERSON, UPDATE THEIR
              CURRENT ROOM */
            int j = 0;
            for(; j < 50; j++)
            {
                if(room_list->rooms_inside[i].client_list[j] != NULL)
                {
                    if(strcmp(room_list->rooms_inside[i].client_list[j]->name, current_->name) == 0)
                    {

                        room_list->rooms_inside[i].num_people--;
                        if(room_list->rooms_inside[i].num_people <= 0)
                        {
                            /* The room is empty we need to clear the room from the list. */
                            room_list->rooms_inside[i].is_used = 0;
                        }
                        else
                        {
                            if(strcmp(room_list->rooms_inside[i].owner_name, current_->name) == 0)
                            {
                                /* We need to promote a new owner */
                                int q = 0;
                                for(; q < 50; q++)
                                {
                                    if(room_list->rooms_inside[i].client_list[q] != NULL)
                                    {
                                        free(room_list->rooms_inside[i].owner_name);
                                        room_list->rooms_inside[i].owner_name = (char*)malloc(strlen(room_list->rooms_inside[i].client_list[q]->name) +1);
                                        strcpy(room_list->rooms_inside[i].owner_name, room_list->rooms_inside[i].client_list[q]->name);
                                        room_list->rooms_inside[i].owner_fd = room_list->rooms_inside[i].client_list[q]->accept_fd;
                                        break;
                                    }
                                }
                            }

                        }
                        room_list->rooms_inside[i].client_list[j] = NULL;
                        current_->current_room = -10;
                        using_rooms = 1;
                        break;
                    }
                }
            }
        }
    }

    while(using_rooms == 0)
    {
        printf("GOT HERE TRYING TO QUERED THE USER.\n");
        pthread_cond_wait(&(room_list->array_ready), &(room_list->array_lock));  
        printf("AFTER THE WAIT \n");
    }
    
    pthread_cond_signal(&(room_list->array_ready));
    pthread_mutex_unlock(&room_list->array_lock);

    return 0;

}


void print_curent_room(int index)
{
    int room = current_client_list->connected_users[index].current_room;

    int i = 0;

    for(; i < max_rooms; i++)
    {
        if(room_list->rooms_inside[i].id == room)
        {
            printf("User: %s is connected to room: %s\n\n\n",current_client_list->connected_users[index].name,
                   room_list->rooms_inside[i].room_name );

        }


    }    


}



/* TODO JOIN A PRIVAE ROOM, IN THE INTEREST OF TIME THIS WILL BE A SEPARATE FUNCTION */

int join_a_room(int room_id, int client_fd)
{
    /* IF THE ROOM ID IS VALID COPY THE USER DATA */

    pthread_mutex_lock(&room_list->array_lock);
    int using_rooms = 0;
    
    printf("In the join room function.\n");

    int i = 1;
    int spot  = 0;

   // int count = room_list->num_in_use;
    for(; i < max_rooms; i++)
    {
        if(room_list->rooms_inside[i].id == room_id)
        {
            int i = 0;
            for(; i < 1000; i++)
            {
                if(current_client_list->connected_users[i].accept_fd == client_fd)
                {
                    int j = 0;
                    for(; j < 100; j++)
                    {
                        if(room_list->rooms_inside[i].client_list[j] == NULL)
                        {
                            room_list->rooms_inside[i].client_list[j] = &current_client_list->connected_users[i];
                            current_client_list->connected_users[i].current_room = room_id;

                            spot = i;
                            using_rooms = 1;
                            break;
                        }
                    }
                }
                if(using_rooms == 1)
                    break;
            }
        }
        if(using_rooms == 1)
            break;
    }

    while(using_rooms == 0)
    {
        printf("GOT HERE TRYING TO QUERED THE USER.\n");
        pthread_cond_wait(&(room_list->array_ready), &(room_list->array_lock));  
        printf("AFTER THE WAIT \n");
    }
    
    pthread_mutex_unlock(&room_list->array_lock);
    pthread_cond_signal(&(room_list->array_ready));
    
    
    print_curent_room(spot);


    if(spot == 0)
    {
        /* IF THE ROOM IS NOT VALID BOUNCE OUT */
        char* error_message_20 = create_error_message(20, "Room was not a valid room or you are already in the room specified.");
        send_to_specific_client(error_message_20, client_fd);

        /* TODO1 bye?*/
    }

    return 0;
}




/* The purpose is to loop through all the rooms and send to the person
   with the appropriate file descriptor. */
int list_all_rooms(int client_fd)
{
    pthread_mutex_lock(&room_list->array_lock);
    int using_rooms = 0;
    
    printf("Before the while loop dicking with the rooms.\n");
    printf("The number of rooms in use: %d\n", room_list->num_in_use);
    int i = 1;

    int count = room_list->num_in_use;
    for(; i < max_rooms; i++)
    {
        printf("IN THE FOR LOOP");
        if(room_list->rooms_inside[i].is_used > 0)
        {
            printf("Count in the list of rooms: %d\n", i);
            //printf("Rooms: %d\n", room_list->rooms_inside[i].is_used);

            if(count == 1)
            {
                char* carriage_ret = " \r\n\r\n";
                char r = '\r';
                char n = '\n';
                char temp[1000];
                memset(temp, '\0', sizeof(temp));
                snprintf(temp, sizeof(temp), "%d", room_list->rooms_inside[i].id);

                /*BUILD MESSAGE TO SEND TO THE CLIENT*/

                char buffer[strlen(room_list->rooms_inside[i].room_name) + strlen(temp) + strlen(carriage_ret)];
            
                snprintf(buffer, strlen(room_list->rooms_inside[i].room_name) + strlen(temp) + strlen(carriage_ret),
                                 "%s %s %c%c%c%c", room_list->rooms_inside[i].room_name,
                                               temp, r,n,r,n);
                
                printf("T HESE ARE THE DRIODS You're LOOKING FOR %s\n", buffer );
                send_to_specific_client(buffer, client_fd);
                

                using_rooms = 1;
            }
            else
            {
                char* carriage_ret = " \r\n";
                char r = '\r';
                char n = '\n';
                char temp[1000];
                memset(temp, '\0', sizeof(temp));
                snprintf(temp, sizeof(temp), "%d", room_list->rooms_inside[i].id);

                /*BUILD MESSAGE TO SEND TO THE CLIENT*/

                char buffer[strlen(room_list->rooms_inside[i].room_name) + strlen(temp) + strlen(carriage_ret)];
            
                snprintf(buffer, strlen(room_list->rooms_inside[i].room_name) + strlen(temp) + strlen(carriage_ret),
                                 "%s %s %c%c", room_list->rooms_inside[i].room_name,
                                               temp, r,n);

                printf("THESE ARE THE DRIODS You're LOOKING FOR %s\n", buffer );
                send_to_specific_client(buffer, client_fd);
                count--;
            }

            
        }
        using_rooms = 1;  
    }



    while(using_rooms == 0)
    {
        printf("GOT HERE TRYING TO QUERED THE USER.\n");
        pthread_cond_wait(&(room_list->array_ready), &(room_list->array_lock));  
        printf("AFTER THE WAIT \n");
    }
    
    pthread_cond_signal(&(room_list->array_ready));
    pthread_mutex_unlock(&room_list->array_lock);

    return 0;
}


/*create ack message*/
char* create_ack_message(char *verb, char* name, char* message)
{
  const void* carriage_return_newline = "\r\n";
  const void* space = " ";
  if(name == NULL && message == NULL)
  {

   /*REMEMBER TO FREE BRUH*/
    char* return_message = malloc((strlen(verb) + strlen(carriage_return_newline)) * sizeof(char));
    memset(return_message, '\0', strlen(verb) + strlen(carriage_return_newline));
    strcpy(return_message, verb);
    strcat(return_message, space);
  	/*return message after putting carriage*/
  	strcat(return_message, carriage_return_newline);
  	printf("THIS IS THE VALUE OF THE RETURN MESSAGE %s\n", return_message);
  	return return_message;
  }
  else if(name != NULL && message == NULL)
  {

   /*REMEMBER TO FREE BRUH*/
	char* return_message = malloc(strlen(name)+ 16);
  	strcpy(return_message, verb);
    strcat(return_message, space);
  	/*there is only a name, so append the name onto the return and then return*/
  	strcat(return_message, name);
    strcat(return_message, space);
  	strcat(return_message, carriage_return_newline);
  	 printf("THIS IS THE VALUE OF THE RETURN MESSAGE %s\n", return_message);
  	return return_message;
  }
  else if(name != NULL && message != NULL)
  {

  	/*REMEMBER TO FREE BRUH*/
  	char* return_message = malloc(strlen(name) + strlen(message) + 16);
  	strcpy(return_message, verb);
    strcat(return_message, space);
  	/*there is both a name and a message*/
  	strcat(return_message, name);
    strcat(return_message, space);
  	strcat(return_message, message);
    strcat(return_message, space);
  	strcat(return_message, carriage_return_newline);

  	 printf("THIS IS THE VALUE OF THE RETURN MESSAGE %s\n", return_message);
  	return return_message;

  }
  else
  {
  	/* you done goof */

  	return NULL;
  }


  /*remember to free, dummy*/

}

int init_room_data_structure(server_chat_rooms_t* room_data)
{
    printf("IN INIT ROOM\n");
    int i = 0;
    printf("MAX_ROOMS NUMBER: %d\n", max_rooms);
    for(; i < max_rooms; i++)
    {
        printf("Within the for loop in init_room_data_structure\n");
    /* self explanatory */
        room_data->rooms_inside[i].room_name = NULL;
        printf("Init Room clear 1\n");
        /* unique id for the room */
         room_data->rooms_inside[i].id = 0;
         printf("Init Room clear 2\n");
        /* current owner of the room */
        room_data->rooms_inside[i].owner_name = NULL;
        printf("Init Room clear 3\n");
        room_data->rooms_inside[i].owner_fd = -1;
        printf("Init Room clear 4\n");
        /* setup for part 3 */
        room_data->rooms_inside[i].is_private = 0;
        printf("Init Room clear 5\n");
        room_data->rooms_inside[i].room_pass = NULL;
        printf("Init Room clear 6\n");
        room_data->rooms_inside[i].is_used = 0;
        printf("Init Room clear 7\n");
        //printf("Room: %s\n",(char*) room_list->rooms_inside[i].client_list[i]);

        room_data->rooms_inside[i].client_list[i] = NULL;
        printf("Init Room clear END\n");
        // int j = 0;
        // for(; j < 100; j++)
        // {
        //     room_list->rooms_inside[i].client_list[j].client_id = 0;

        //     /* nickname */
        //     room_list->rooms_inside[i].client_list[j].name = NULL; 

        //     //thread_fds_t descriptors;

        //     room_list->rooms_inside[i].client_list[j].accept_fd = 0;

        //     /* current chat room the client is logged in */
        //     room_list->rooms_inside[i].client_list[j].current_room = 0;

        //     room_list->rooms_inside[i].client_list[j].session_id = 0;

        //     /* we are going to periodically ping the clients to
        //        see if any have disconnected improperly. */
        //    room_list->rooms_inside[i].client_list[j].still_connected = 0;
        //     /* time recorded when the client logs in */
        //     room_list->rooms_inside[i].client_list[j].log_in_time = 0;

        //     /*status flag*/
        //    room_list->rooms_inside[i].client_list[j].status_flag = 0;

        //     /*new user flag*/
        //     room_list->rooms_inside[i].client_list[j].new_user_flag = 0;      
        // }


    }

    printf("After initial assignment in the room loop.");

    room_data->next_available = 1;
    room_data->num_in_use = 0;

    printf("AFTER INITIAL ASSIGNMENT\n");
        /* self explanatory */

         char* name = "Waiting Room";
         room_data->rooms_inside[0].room_name = (char*)malloc((strlen(name)+1)*sizeof(char));
         memset(room_data->rooms_inside[0].room_name, '\0', (strlen(name)+1));
         strcpy(room_data->rooms_inside[0].room_name, name);


         char* server_name = "Server";
         room_data->rooms_inside[0].owner_name = (char*)malloc((strlen(server_name)+1)*sizeof(char));
         memset(room_data->rooms_inside[0].owner_name , '\0', (strlen(server_name)+1));
         strcpy(room_data->rooms_inside[0].owner_name, server_name);

        // /* unique id for the room */
          room_data->rooms_inside[i].id = -10;

         room_data->rooms_inside[i].owner_fd = -10;

        // /* setup for part 3 */
         room_data->rooms_inside[i].is_private = 0;

         room_data->rooms_inside[i].room_pass = NULL;

         room_data->rooms_inside[i].is_used = 1;

        pthread_mutex_init (&(room_data->array_lock), NULL);
        pthread_cond_init (&(room_data->array_ready), NULL);

        pthread_mutex_unlock(&(room_data->array_lock));
        pthread_cond_signal(&(room_data->array_ready));

    return 0;
}


int init_client_data_structure(struct connected_clients* some_user_data)
{
    int i = 0;
    for(; i < 1000; i++)
    {
        some_user_data->connected_users[i].client_id = 0;

        /* nickname */
        some_user_data->connected_users[i].name = NULL; 

        //thread_fds_t descriptors;

        some_user_data->connected_users[i].accept_fd = 0;

        /* current chat room the client is logged in */
        some_user_data->connected_users[i].current_room = 0;

        some_user_data->connected_users[i].session_id = 0;

        /* we are going to periodically ping the clients to
           see if any have disconnected improperly. */
        some_user_data->connected_users[i].still_connected = 0;
        /* time recorded when the client logs in */
        some_user_data->connected_users[i].log_in_time = 0;

        /*status flag*/
        some_user_data->connected_users[i].status_flag = 0;
        printf("The value of the status flag in the init structure %d\n",  some_user_data->connected_users[i].status_flag);

        /*new user flag*/
        some_user_data->connected_users[i].new_user_flag = 0; 

    }



    pthread_mutex_init (&(some_user_data->array_lock), NULL);
    pthread_cond_init (&(some_user_data->array_ready), NULL);

    pthread_mutex_unlock(&(some_user_data->array_lock));
    pthread_cond_signal(&(some_user_data->array_ready));


    return 0;
}
//return 0

