#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>

#define ERROR_MESSAGE   "\x1B[1;31m"
#define INFORMATIVE_MESSAGE     "\x1B[1;34m"
#define PRIVATE_MESSAGE  "\x1B[1;35m"
#define END_MESSAGE   "\x1B[0m"

#define CLIENT 10
#define SERVER 7

/* Methods to manually verify that we have a proper ipv4 address. */
int valid_digit(char* ip_address);
int is_valid_ip(char* ip_address);

char* help(int which);
int check_if_port_alright(int port);

int set_socket_keep_alive(int* file_descriptor);


    /**
     * Print out the program usage string for the client. 
     */
    #define HELP_CLIENT(name) do {                                                                                                                                    \
        fprintf(stdout, INFORMATIVE_MESSAGE                                                                                                                                               \
            "\n%s [-h] \n"                                                                                                                                            \
            "Client program for the Hangry implementation of the PIRC chat protocol.\n"                                                                               \
            "Format for command line: ./client NAME SERVER_IP SERVER_PORT .\n"                                                                                        \
            "\n"                                                                                                                                                      \
            "Option arguments:\n\n"                                                                                                                                   \
            "-h                             Displays this help menu.\n"                                                                                               \
            "-c:                            Specifies that you are a new user.\n Notify the server to create a new registered user.\n"                                \
            "\nPositional arguments:\n\n"                                                                                                                             \
            "NAME                           Name of the user.\n"                                                                                                      \
            "SERVER_IP                      Destination IP address of the target PIRC server.\n"                                                                      \
            "SERVER_PORT                    The listeing port of the target PIRC server.\n"                                                                           \
            "\nCommands:\n\n"                                                                                                                                         \
            "/quit                          Format: /quit             			Result: Ends the hangry PIRC client.\n"                                               \
            "/creater                       Format: /creater <name>   			Result: Requests a chat room with the specified name.\n"                              \
            "/listrooms                     Format: /listrooms        			Result: Requests a list of all current rooms in the PIRC server.\n"                   \
            "/kick                          Format: /kick <username>  			Result: The owner of a chat room may remove the specifed user.\n"                     \
            "/join                          Format: /join <id>        			Result: Join chat room specified by the supplied id.\n"                               \
            "/leave                         Format: /leave            			Result: Leaves the current chat room and brings the user to the waiting room.\n"      \
            "/tell                          Format: /tell <username> <message>  		Result: Send private message to another user in the room.\n"                          \
            "/listusers                     Format: /listusers                  		Result: Returns a list of all users in the current chat room.\n"                      \
            "/help                          Format: /help 					Result: Displays help menu.\n"                                                        \
            END_MESSAGE"\n"                                                                                                                                                      \
            ,(name)                                                                                                                                                   \
        );                                                                                                                                                            \
    } while(0)    


    #define HELP_SERVER(name) do {                                                                                                \
        fprintf(stdout,                                                                                                     \
            "\n%s [-h] \n"                                                                           \
            "\n"                                                                                                            \
            "Server program for the Hangry implementation of the PIRC chat protocol.\n"                                        \
            "Format for command line: ./server PORT_NUMBER MOTD .\n"                                        \
            "\n"                                                                                                            \
            "Option arguments:\n\n"                                                                                         \
            "-h                             Displays this help menu.\n"                                                    \
            "-N: num                        Specifies the maximum number of chat rooms. If not supplied the default is 5.\n                              g Notify the server to create a new registered user.\n"    \
            "-e                             Echo messages received on server.\n"                                                                                         \
            "\nPositional arguments:\n\n"                                                                                   \
            "PORT_NUMBER                    Specifies the listening port.\n"                                              \
            "MOTD                           Message to display to a client when they connect.\n"                                           \
            "\nCommands:\n\n"                                                                                   \
            "\n"                                                                                                            \
            ,(name)                                                                                                         \
        );                                                                                                                  \
    } while(0)
     
#endif

void newline_remove(char* word);