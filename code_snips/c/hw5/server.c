#include "list_stuff.h"
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include "server.h"
#include "chat_functions.h"

/* Client list */

//static some_node* client_list;
/*The almighty mutex */
pthread_mutex_t lock;
/* Going to hold our properly connected clients. */
struct registered_users client;



/* Array of listening threads. */
pthread_t threads[MAX_THREADS];

void init()
{
	client.that_thing = 0;
	pthread_mutex_init(&lock, NULL);
}

int register_client_node(int socket)
{
	if(client.that_thing == MAX_USERS)
		return 1;
	
	int temp = client.that_thing + 1;
	client.file_descriptors[temp] = socket;
	client.that_thing = temp;

	return 0;
}

void send_to_clients(char* out_message)
{
	pthread_mutex_lock(&lock);

	struct usr_message message;

	message.type = server_msg;
	strncpy(message.message, out_message, sizeof(message.message));

	int i;	
	for(i = 0; i < client.that_thing; i++)
	{
		send(client.file_descriptors[i], &message, sizeof(message), 0);
	}

	pthread_mutex_unlock(&lock);
}

void* receive_messages(int client_socket)
{
	struct usr_message message;
	int socket = client_socket;

	printf("Hello there recieve_messages! I want to sleep. \n");

	while(1)
	{
		int size = recv(socket, &message, sizeof(message), 0);
		
		if(message.type == client_to_client && size > 0)
		{
			/* We can check for an echo flag here to see in case we need to repeat the message on the server end. */
			char test[500];
			snprintf(test, sizeof(test), "THIS IS A TEST: %s, %s", message.name, message.message);
			send_to_clients(test);
		}
	}
	return NULL;
}

void  create_thread(thread_fds_t* descriptors, size_t num_threads)
{
	printf("Create Threads! \n");
	unsigned int i;
	for(i = 0; i < num_threads; ++i)
	{
		pthread_create(&threads[i], NULL, &connection_handler, (void*)descriptors);
	}
}

void* connection_handler(void* conn)
{
	thread_fds_t descriptors = *((thread_fds_t*) conn);
	struct sockaddr_in client;
	socklen_t size = sizeof(client);

	printf("Hello there connection_handler! Are you tired today?\n");
	
	while(1) 
	{
		struct epoll_event* events = (struct epoll_event*)malloc(MAX_THREADS_IN_USE * sizeof(struct epoll_event));
		int epoll_num_returned = epoll_wait(descriptors.fd, events , MAX_EPOLL_EVENTS_PER, -1);

		if(epoll_num_returned < 0)
		{
			perror("epoll_wait failed");
			return (void*) FAILURE;
		}

		int i;
		for( i = 0; i < epoll_num_returned; ++i)
		{
			if(events[i].data.fd == descriptors.socket)
			{ 
				/* We have a new connection. */
				int client_socket = accept(events[i].data.fd, (struct sockaddr*)&client, &size);
				if(client_socket == -1 )
				{
					perror("failed");
					continue;
				}
				struct epoll_event event;
				event.data.fd = client_socket;

				event.events = EPOLLIN | EPOLLET;
				//event.events = EPOLLIN;
				if(epoll_ctl(descriptors.fd, EPOLL_CTL_ADD, client_socket, &event) == -1)
				{
					perror("Fucked adding the fd.");
					continue;
				}
			}
			else
			{ 
				char* buffer = (char*) malloc(2000);	
				char* position = buffer;
				ssize_t bytes_read = 0;
				int total = 0;

				while(bytes_read < 2000)
				{
					printf("this is hell\n");
					printf("number of bytes total: %d\n" ,total);
					if((bytes_read = read(events[i].data.fd, position, 2000 - (position - buffer))) > 0)
					{
						position += bytes_read;
						total += bytes_read;

						printf("number of bytes read: %zu\n" ,bytes_read);
						printf("number of bytes read: %d\n" ,total);
					}
					else
					{
						perror("Error on read client socket");
						continue;
					}
					if(*(buffer + total) == '\0')
						break;

					printf("what the duce?\n");
				}


				printf("what is this hell?????\n");

				// if(bytes_read > 0)
				// {
				// 	printf("bytes_read entered\n");
				// 	printf("number of bytes read: %zu\n" ,bytes_read);
					char* message = (char*) malloc(bytes_read);
					memcpy(message, buffer, bytes_read);
					write(1, message, strlen(message));
					printf("The next line cli_message\n");
					// write(1, ((cli_message*)buffer)->content, strlen(((cli_message*)buffer)->content));
					// printf("I enjoy this class very much right now\n");
					// switch(message->type)
					// {
					// 	case msg_connect:
					// 		printf("Message connect \n");
					// 		register_user(message, events[i].data.fd);
					// 		break;
					// 	case msg_message:
					// 		write(1,"MESSAGE \n", 19);
					// 		receive_messages(events[i].data.fd);
					// 	/* Need to implement other message types */
					// 	default:
					// 		break;
					// }



					free(message);
				// }


				free(buffer);
			}
		}
		free(events);
	}
	return (void*)SUCCESS;
}

/*void register_user(cli_message* message, int client_socket)
{
	client_t some_client;
	some_client.descriptors.socket = client_socket;
	some_client.name = (char*) malloc(message->length+1);
	some_client.name[message->length] = '\0';
	strncpy(some_client.name, message->content, message->length);
*/	
	/* We are going to generate session id's here later. */
	/* Also need to generate some unique ids. */
/*	some_client.client_id = 0;
	
	some_node* new_node = make_new_node();
	
	new_node->stuff_that_goes_in_the_node  = &some_client;
	if(!client_list)
	{
		client_list = init_list();
	}
	push_back(&client_list, new_node);
}*/

int main(int argc, char ** argv, char **envp)
{
	int server_socket;
	struct sockaddr_in server;

	init();
	server_socket = socket(PF_INET, SOCK_STREAM, 0);

	if(server_socket == -1)
	 {
		perror("Failed to create the initial socket. Died in a fire.");
		exit(EXIT_FAILURE);
	}

	memset(&server, 0, sizeof(server));

	server.sin_family = PF_INET;
	server.sin_port = htons(4567);
	server.sin_addr.s_addr = INADDR_ANY;

	if(bind(server_socket, (struct sockaddr *)&server, sizeof(server)) == -1)
	{
		perror("Binding the server failed. Fucked.");
		exit(EXIT_FAILURE);
	}

	if(listen(server_socket, MAX_LISTENERS) == -1)
	{
		perror("Can't listen on the server socket.");
		exit(EXIT_FAILURE);
	}

	int epoll_server = epoll_create(1); 
	if(epoll_server < 0)
	{
		perror("Can't creat epoll?");
		exit(EXIT_FAILURE);
	}

	struct epoll_event some_event;
	some_event.events = EPOLLIN; 
	some_event.data.fd = server_socket;

	if(epoll_ctl(epoll_server, EPOLL_CTL_ADD, server_socket, &some_event))
	{
		perror("Epoll control interface failed.");
		exit(EXIT_FAILURE);
	}

	thread_fds_t descriptors;
	descriptors.socket = server_socket;
	descriptors.fd = epoll_server;
	create_thread(&descriptors, MAX_THREADS);

	int i;
	for(i = 0; i < MAX_THREADS; ++i)
	{
		if(threads[i])
		{
			if(pthread_join(threads[i], NULL) == 0)
			{
				/* Maybe add a print statement for diagnostic purposes. */
			}
		}
	}

	close(server_socket);
	return EXIT_SUCCESS;

	printf("%d\n", argc );

	printf("%s\n", *argv );

	printf("%s\n", *envp );

	//return 0;
} 

/*check the message that was recieved and do the  appropriate action*/
int check_recieved_message(char *recieved_message, int client_fd)
{

	/* tokenize */
	char *token_name;
    token_name = strtok(recieved_message, " ");

    //token contains the rest of the string
    //recieved_message contains the token

    const void* aloha_cmd = "ALOHA!";
    const void* iam_cmd = "IAM";
    const void* msg_cmd = "MSG";
    const void* iamnew_cmd = "IAMNEW";
    const void* creater_cmd = "CREATER";
    const void* listr_cmd = "LISTR";
    const void* kick_cmd = "KICK";
    const void* join_cmd = "JOIN";
    const void* leave_cmd = "LEAVE";
    const void* tell_cmd = "TELL";
    const void* listu_cmd = "LISTU";

    void* ahola_cmd = "AHOLA! ";
    void* hinew_cmd = "HINEW ";
    // void* new_pass_cmd = "NEWPASS";
    // void* retaerc_cmd = "RETAERC ";
    // void* rtsil_cmd = "RTSIL";
    // void* nioj_cmd = "NIOJ";
    // void* evael_cmd = "EVAEL";
    // void* llet_cmd = "LLET";
    // void* echop_cmd = "ECHOP";
    // void* utsil_cmd = "UTSIL";


    //cmd checking
    	if(strcmp(recieved_message, aloha_cmd) == 0)
    	{
    		//client attempting to connect

    		//do some stuff

    		//make a message
    		create_ack_message(ahola_cmd, NULL, NULL);

    		//send the message back

    	}
    	else if(strcmp(recieved_message, iam_cmd) == 0)
    	{
    		//client trying to identify a username

    		//do some stuff

    		//make a message
    		create_ack_message(hinew_cmd, token_name, NULL);

    		//send the message back

    	}
    	else if(strcmp(recieved_message, msg_cmd) == 0)
    	{
    		//client trying to send a private message

    	}
    	else if(strcmp(recieved_message, iamnew_cmd) == 0)
    	{
    		//register a new username

    	}
    	else if(strcmp(recieved_message, creater_cmd) == 0)
    	{
    		//attempt to make a new room

    	}
    	else if(strcmp(recieved_message, listr_cmd) == 0)
    	{
    		//wants a list of all the rooms with name and ids

    	}
    	else if(strcmp(recieved_message, kick_cmd) == 0)
    	{
    		//user wanted to kick someone out of a room
    	}
    	else if(strcmp(recieved_message, join_cmd) == 0)
    	{
    		//user wants to join the room, take care of this

    	}
    	else if(strcmp(recieved_message, leave_cmd) == 0)
    	{
    		//user leaves chat and goes to the waiting room

    	}
    	else if(strcmp(recieved_message, tell_cmd) == 0)
    	{
    		// tell command

    	}
    	else if(strcmp(recieved_message, listu_cmd) == 0)
    	{
    		//list of users

    	}
    	else
    	{
    		//goof up?
    		
    	}

	return 0;
}


/*create error message*/
//ERR 30 <message> \r\n

char* create_error_message(int error_number)
{
	const void* err_msg = "ERR ";
	const void* carriage_return_newline = "\r\n";

	const void* sorry_msg = " SORRY";
	const void* user_exist_msg = " USER EXISTS";
	const void* does_not_exist_msg = " DOES NOT EXIST";
	const void* room_exist_msg = " ROOM EXISTS";
	const void* maximum_rooms_reached_msg = " MAXIMUM ROOMS REACHED";
	const void* room_does_not_exist_msg = " ROOM DOES NOT EXIST";
	const void* user_not_present_msg = " USER NOT PRESENT";
	const void* not_owner_msg = " NOT OWNER";
	const void* invalid_use_msg = " INVALID USE";
	const void* invalid_operation_msg = " INVALID OPERATION";
	const void* invalid_password_msg = " INVALID PASSWORD";
	const void* internal_server_error_msg = " INTERNAL SERVER ERROR";
	const void* you_messed_up = " YOU MESSED UP";

	/*REMEMBER TO FREE BRUH*/
    char* return_message = malloc(64);
    strcpy(return_message, err_msg);

    char buffer[8] = " ";
	int place = error_number;

	sprintf(buffer, "%d", place);

	/*append the error code*/
	strcat(return_message, buffer);

	if(error_number == 00)
	{
		//00 SORRY 
  		/*append the message*/
  		strcat(return_message, sorry_msg);
  		strcat(return_message, carriage_return_newline);
  		return return_message;

	}
	else if(error_number == 01)
	{
		//01 USER EXISTS 
		strcat(return_message, user_exist_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
		
	}
	else if(error_number == 02)
	{
		//02 DOES NOT EXIST 
		strcat(return_message, does_not_exist_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 10)
	{
		//10 ROOM EXISTS 
		strcat(return_message, room_exist_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 11)
	{
		//11 MAXIMUM ROOMS REACHED 
		strcat(return_message, maximum_rooms_reached_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 20)
	{
		//20 ROOM DOES NOT EXIST 
		strcat(return_message, room_does_not_exist_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 30)
	{
		//30 USERNOT PRESENT
		strcat(return_message, user_not_present_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;

	}
	else if(error_number == 40)
	{
		//40 NOT OWNER 
		strcat(return_message, not_owner_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 41)
	{
		//41 INVALID USE
		strcat(return_message, invalid_use_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 60)
	{
		//60 INVALID OPERATION 
		strcat(return_message, invalid_operation_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 61)
	{
		//61 INVALID PASSWORD 
		strcat(return_message, invalid_password_msg);
		strcat(return_message, carriage_return_newline);
		return return_message;
	}
	else if(error_number == 100)
	{
		//The default error message for any other conditions which were not mentioned:
    	//ERR 100 INTERNAL SERVER ERROR
    	strcat(return_message, internal_server_error_msg);
    	strcat(return_message, carriage_return_newline);
    	return return_message;
	}
	else
	{
		// error message number was bad
		strcat(return_message, you_messed_up);
		strcat(return_message, carriage_return_newline);
		return return_message;

	}
}

/*create ack message*/
char* create_ack_message(char *verb, char* name, char* message)
{

  const void* carriage_return_newline = "\r\n";

  if(name == NULL || message == NULL)
  {

   /*REMEMBER TO FREE BRUH*/
    char* return_message = malloc(16);
    strcpy(return_message, verb);

  	/*return message after putting carriage*/
  	strcat(return_message, carriage_return_newline);
  	return return_message;
  }
  else if(name != NULL && message == NULL)
  {

   /*REMEMBER TO FREE BRUH*/
	char* return_message = malloc(sizeof(name)+ 16);
  	strcpy(return_message, verb);

  	/*there is only a name, so append the name onto the return and then return*/
  	strcat(return_message, name);
  	strcat(return_message, carriage_return_newline);
  	return return_message;
  }
  else if(name != NULL && message != NULL)
  {

  	/*REMEMBER TO FREE BRUH*/
  	char* return_message = malloc(sizeof(name) + sizeof(message) + 16);
  	strcpy(return_message, verb);

  	/*there is both a name and a message*/
  	strcat(return_message, name);
  	strcat(return_message, message);
  	strcat(return_message, carriage_return_newline);
  	return return_message;

  }
  else
  {
  	/* you done goof */

  	return NULL;
  }

  /*remember to free, dummy*/

}