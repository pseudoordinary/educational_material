#include <stdlib.h>
#include "chat_functions.h"
#include "list_stuff.h"
#include <stdio.h>
#include "server.h"

/* sets up the list and returns the newly created head */
inline some_node* init_list()
{
	some_node* head = make_new_node();
	head->head = head;
	return head;
}

/* creates a new node and initializes the parameters NULL */
inline some_node* make_new_node()
{
	some_node* head = (some_node*) malloc(sizeof(some_node));
	head->next = NULL;
	head->prev = NULL;
	head->stuff_that_goes_in_the_node = NULL;
	return head;
}

/* kills the list and cleans it up */
int destroy_list(some_node** head)
{
	size_t temp = 0;
	temp = num_list_elements(*head); 
	while(temp > 0 )
	{
		some_node* node_to_be_removed = (*head);
		(*head) = (*head)->next;

		/* Going to have to modify this if we decided to dynamically allocate
		   memory for any nodes. */
		free(node_to_be_removed);
		temp = num_list_elements(*head);
	}
	return SUCCESS;
}

/* pushes a new node to the front of the list */
int push_front(some_node** head, some_node* new_head)
{
	//printf("THE VALUE OF THE HEAD OF THE LIST %p\n", (*head)->client_list );
	printf("THE VALUE OF THE NEW HEAD POINTER %s\n", ((client_t*)(new_head->stuff_that_goes_in_the_node))->name );
	if(!*head)
		return FAILURE;

	printf("DID WE GET HERE \n");
	printf("DEFERENCE OLD HEAD:  %p\n", (*head));
	new_head->next = (*head);

	printf("THIS IS THE VALUE OF HEAD OREVIOUS BEFORE ASSIGNEMNT %p\n", (*head)->prev );
	(*head)->prev = new_head;
	*head = new_head;
	(*head)->head = *head;
	
	return SUCCESS;
}

/* pushes a new node to the back of the list */
int push_back(some_node** head, some_node* new_tail)
{
	//printf("I got into the push_back funtion\n");
	if(!*head)
	{
		printf(" If !*head \n");
		return FAILURE;

	}	
	printf("AFTER IF \n");
	some_node* temp = (*head)->head;
	
	printf("Before the while loop, assignment for temp is okay. :D ");
	while(temp->next)
	{	
		temp = temp->next;
		printf("temp value: %p\n", temp);
		printf("temp value for next: %p\n", temp->next);
	}
	
	printf("After while loop \n");
	temp->next = new_tail;
	printf("new_tail: %p\n", new_tail);
	new_tail->prev = temp;
	printf("new_tail->prev: %p\n", new_tail->prev);
	printf("new_tail->prev = temp, what is temp? %p\n", temp);
	new_tail->head = temp;
	printf("Assignment of new_tail is complete\n");
	
	printf("THE NAME OF THE PERSON IS %s\n", ((client_t*)new_tail)->name);

	return SUCCESS;
	printf("SHould not be here\n");
}

/* returns the number of elements in the list */
size_t num_list_elements(some_node* head)
{
	size_t size = 0;
	some_node* temp = head;
	
	while(temp)
	{
		size++;
		temp = temp->next;
	}
	return size;
}

/* Returns a node by absolute position in the list */
some_node* get_node_by_index(const some_node* head, unsigned int index)
{
	some_node* temp = head->head;
	size_t current_pos = 0;
	while(current_pos != index)
	{
		temp = temp->next;
		current_pos++;
	}
	return temp;
}

/* Returns a node by the associated client id */
/*some_node* get_node_by_client_id(const some_node* head, unsigned int client_id)
{
	some_node* temp = NULL;
	return temp;

}

*/
/* Removes a node by absolute position in the list */
int remove_node_by_index(some_node** head, size_t position)
{
	some_node* temp = *head;
	some_node* node_to_be_removed = NULL;
	
	/* Remove the head */
	if(position == 0)
	{ 
		temp = (*head)->next;
		free(*head);
		*head = temp;

		return SUCCESS;
	}

	size_t count = 0;
	while (count != position)
	{
		if(!temp)
		{ 
			return FAILURE;
		}
		temp = temp->next;
		count++;
	}

	if(temp)
	{ 
		node_to_be_removed = temp;
		if(node_to_be_removed->prev)
			node_to_be_removed->prev->next = node_to_be_removed->next;
		if(node_to_be_removed->next)
			node_to_be_removed->next->prev = node_to_be_removed->prev;
		
		free(node_to_be_removed);
		return SUCCESS;
	}
	return FAILURE;
}

/* Removes a node by the associated client id */
/*int remove_node_by_id(some_node** head, unsigned int client_id)
{
	return 0;
}

*/