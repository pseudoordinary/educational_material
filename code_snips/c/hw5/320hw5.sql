/*PIRC*/

drop database if exists PIRC_USERS;
create database PIRC_USERS;
use PIRC_USERS;

CREATE TABLE Users (
  UserID INTEGER AUTO_INCREMENT,
  Username VARCHAR(254) NOT NULL,
  Password VARCHAR(254),
  Salt INTEGER,
  PRIMARY KEY(UserID));

DELIMITER $$

# Add User
CREATE PROCEDURE addUser(IN name VARCHAR(254), IN pass VARCHAR(254), IN NaCl INTEGER)
BEGIN
insert into PIRC_USERS.Users(Username, Password, Salt)
  values(name, pass, NaCl);
End $$

#Delete USER
CREATE PROCEDURE deleteUser(In name VARCHAR(254))
BEGIN
  DELETE FROM Users WHERE Username = name;
End $$

#Edit USER
CREATE PROCEDURE editUser(IN name VARCHAR(254), IN pass VARCHAR(254), IN NaCl INTEGER)
BEGIN
 UPDATE Users
 SET Username = name, Password = pass, Salt = NaCl
 WHERE Username = name;
End $$

#Get password with the username
CREATE PROCEDURE getPasswordWithUsername(in name VARCHAR(254))
BEGIN
  SELECT U.Password
  FROM Users U
  WHERE  U.Username = name;
End $$

#Get the salt with the username
CREATE PROCEDURE getSaltWithUsername(in name VARCHAR(254))
BEGIN
  SELECT U.Salt
  FROM Users U
  WHERE  U.Username = name;
End $$

#Get the userID with the username
CREATE PROCEDURE getUserIDWithUsername(in name VARCHAR(254))
BEGIN
  SELECT U.UserID
  FROM Users U
  WHERE  U.Username = name;
End $$

DELIMITER ;