#ifndef __CLIENT_H__
#define __CLIENT_H__

#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
//#include "commands.h"*/
#include "chat_functions.h"
#include <errno.h>


#define UNUSED(x) (void)(x)
#define BAD_NUM_ARGS -1
#define KILLER_EXIT -5
#define HELP -2

struct init_cmd_args
{
	//username
	char* username;

	//ip
	char* ip_address;

	//port num
	int port_num;
}; 

typedef struct message_information
{
	int quit_type;
	int iam_type;
	int iamnew_type;
	int creater_type;
	int listr_type;
	int kick_type;
	int join_type;
	int leave_type;
	int tell_type;
	int listu_type;
}message_info_t;



/* Used to create an inital connection */
//cli_message* connect_msg(char* name);
//size_t message_size(const cli_message* message);

/* WHere the chat magic happens. */
void* get_message_from_usr(void* socket);

/*make the messages to be sent over from the client*/
char* create_sending_message(char *verb, char* name, char* message);

/*gets all the positional arguments, puts in a struct*/
int opted(int argc, char*argv[]);

/*check if port is in a acceptable range*/
int check_if_port_alright(int port);

int check_instruction(char *cmd_client, int* file_descriptor);

//void newline_remove(char *string);

int opt_server(int argc, char* argv);

int check_acknowledgement_squiggly(char* buf1);

int Make_Message_And_Send( char* verb , char* namepass, char* message, int* file_des);

#endif