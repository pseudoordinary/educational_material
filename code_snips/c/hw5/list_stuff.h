#ifndef __LIST_STUFF_H__ 
#define __LIST_STUFF_H__

#include <sys/types.h>


struct node
{
	/* Standard list stuff */
	struct node* next;
	struct node* prev;
	struct node* head;
	/* Not sure if we need some kind payload
	   but it may come in handy. */
	void* stuff_that_goes_in_the_node;
};

typedef struct node some_node;

/* sets up the list */
some_node* init_list(void);

/* creates a new node */
some_node* make_new_node(void);

/* kills the list and cleans it up */
int destroy_list(some_node** head);

/* pushes a new node to the front of the list */
int push_front(some_node** head, some_node* new_head);

/* pushes a new node to the back of the list */
int push_back(some_node** head, some_node* new_tail);

/* returns the number of elements in the list */
size_t num_list_elements(some_node* head);

/* Returns a node by absolute position in the list */
some_node* get_node_by_index(const some_node* head, unsigned int index);

/* Returns a node by the associated client id */
//some_node* get_node_by_client_id(const some_node* head, unsigned int client_id);

/* Removes a node by absolute position in the list */
int remove_node_by_index(some_node** head, size_t index);

/* Removes a node by the associated client id */
//int remove_node_by_id(some_node** head, unsigned int client_id);

#endif