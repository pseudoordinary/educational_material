#ifndef __SERVER_H__
#define __SERVER_H__

#include <pthread.h>
#include <time.h>
#include <sqlite3.h>
#include "common.h"

#define MAX_EPOLL_EVENTS_PER 100
/* This will defintiely need to be changed later */
#define MAX_THREADS_IN_USE 1
#define BUFFER_SIZE 1001
#define MAX_LISTENERS 100

/* This will definitely need to change. */
#define MAX_THREADS 50

#define UNUSED(x) (void)(x)
#define SQUIGGLESQUIGGLE 50

#define USER_NOT_IN_DB -6
#define USER_LOGGED_IN -7


typedef struct
{
	/* unique id for the client. */
	unsigned int client_id; 
	/* nickname */
	char* name; 

	//thread_fds_t descriptors;
	
	int accept_fd;

	/* current chat room the client is logged in */
	int current_room;
	
	size_t session_id;

	/* we are going to periodically ping the clients to
	   see if any have disconnected improperly. */
	size_t still_connected;
	/* time recorded when the client logs in */
	time_t log_in_time;

	/*status flag*/
	int status_flag;

	/*new user flag*/
	int new_user_flag;
	
} client_t;

struct connected_clients
{
	pthread_mutex_t array_lock;
    pthread_cond_t array_ready;

    client_t connected_users[1000];

    //some_node* client_list;
};
 


typedef struct thread_kille_strct
{
	pthread_mutex_t kill_lock;
    pthread_cond_t kill_ready;

	pthread_t id;
	int fd;
}kill_t;

typedef struct worker
{
        void *(*process)(void *arg, char* buf);
        void *arg;
        char buf[1001];
        struct worker *next;
}thread_worker_t;

typedef struct
{
    pthread_mutex_t queue_lock;
    pthread_cond_t queue_ready;
    
    thread_worker_t *queue_head;
   
    int shutdown;
    pthread_t* thread_id;
    
    int max_thread_num;
    int current_queue_size;
}thread_pool_t;

typedef struct param
{
        int *epoll_fd;
        int *accept_fd;
        int *maxfd;
        struct epoll_event *ev;
        struct connected_clients* ptr;
                
}thread_params_t;

typedef struct
{
	/* socket used to communicate with the client */
	int socket;
	/* fd for i/o */
	int fd;

} thread_fds_t; 


typedef struct
{
	/* self explanatory */
	char* room_name;
	
	/* unique id for the room */
	int id;

	/* list of currently connected clients in the room */
	client_t* client_list[50];

	/* current owner of the room */
	char* owner_name;

	int owner_fd;

	/* setup for part 3 */
	int is_private;

	char* room_pass;

	int num_people;

	int is_used;

} chat_room_t;

typedef struct server_cmd_args
{
	//max_chats
	int max_chat_room;
	//port
	int port_num;
	//message of the day
	char* motd;
	int echo_flag;
} server_cmd_args; 


typedef struct chat_rooms
{
	pthread_mutex_t array_lock;
    pthread_cond_t array_ready;

    chat_room_t rooms_inside[1000];
    int next_available;
    int num_in_use;

    //some_node* client_list;
}server_chat_rooms_t;

/* Sets up our listening threads. */
void create_thread(thread_fds_t* thread_fds, size_t num_threads);

/**This function handle with incomming connection. */
void* connection_handelr(void* server_socket);

/* Registers a user with the server. */
void register_user(char* message, int client_socket, int status);

int init_client_data_structure(struct connected_clients* some_user_data);


/* Registers a node. */
int register_client_node(int socket);

void init(void);

void send_to_clients(char* out_message);

void* receive_messages(int client_socket);

void  create_thread(thread_fds_t* descriptors, size_t num_threads);

void* connection_handler(void* conn);

int check_recieved_message(char *recieved_message, int client_fd, thread_params_t* params);

char* create_error_message(int error_number, char* name);

char* create_ack_message(char *verb, char* name, char* message);
int init_room_data_structure(server_chat_rooms_t* room_data);
client_t* get_user_info(int client_fd);
int join_a_room(int room_id, int client_fd);


int create_room(char* room_name, int client_fd);
int list_users_in_room(int client_fd);

int kick_user_from_room( char* target , int client_fd);

int leave_a_room(int client_fd);

int list_all_rooms(int client_fd);

#endif