#include "database_interface.h"
#include <sqlite3.h>


static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
   int i;
   for(i=0; i<argc; i++){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}


int main(int argc, char ** argv, char **envp)
{
	
  //struct database_connection sql_database;


   char* error_msg;
	char* sql_statement;
   int ret;

   sqlite3* database;
   int rc;

   rc = sqlite3_open("chat_server_v5.db", &database);

   if(rc)
   {
      printf("Can't open database: %s\n", sqlite3_errmsg(database));
      exit(0);
      return -1;
   }
   else
   {
      printf("Opened database successfully\n");
   }
    
   /* Ued to create the user table */
	sql_statement = "CREATE TABLE Users(" \
					  "UserID INTEGER PRIMARY KEY AUTOINCREMENT," \
					  "Username VARCHAR(254) NOT NULL UNIQUE," \
					  "Password VARCHAR(254)," \
					  "Salt VARCHAR(254));";


   ret = sqlite3_exec(database, sql_statement, callback, 0, &error_msg);
   if( ret != SQLITE_OK ){
   fprintf(stderr, "SQL error: %s\n", error_msg);
      sqlite3_free(error_msg);
   }else{
      fprintf(stdout, "Table created successfully\n");
   }

   // user_db_info sample;
   // sample.username = "Wong";
   // //sample.password =  //"KachunkKachunk123";
   // //sample.salt = 373;

   // //insert_user_to_db(database, &sample);

   // query_user(database, &sample);

}