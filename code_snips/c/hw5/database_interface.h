#ifndef __DATABASE_INTERFACE_H__
#define __DATABASE_INTERFACE_H__

#include <string.h>
#include <sqlite3.h>
#include <stdlib.h>
#include <stdio.h>


struct database_connection
{
	pthread_mutex_t list_lock;
    pthread_cond_t list_ready;

    sqlite3* database;
};

typedef struct user_db_info
{
	char* username;
	char* password;
	char* salt;
}user_db_info; 

int validate_pass(char* const password_input);
int init_db_connection(struct database_connection* sql_database);
int insert_user_to_db(sqlite3* database, user_db_info* client_stuff);
int query_user(sqlite3* database, char* username);
int get_client_data(sqlite3* database, user_db_info* client);

#endif